FROM rust:alpine

RUN apk update

RUN apk add --no-cache python3 
RUN apk add --no-cache python3-dev 
RUN apk add --no-cache musl-dev 

# needed due to breaking change in 1.78 for ahash crate
RUN rustup toolchain install nightly-2024-01-18-x86_64-unknown-linux-musl 
RUN rustup default nightly-2024-01-18-x86_64-unknown-linux-musl

RUN mkdir -p /opt/pagan-rs/


RUN mkdir -p /opt/bin/

COPY . /opt/pagan-rs/


WORKDIR /opt/pagan-rs/

RUN cargo build --release --workspace

RUN cp ./target/release/migration /opt/bin/
RUN cp ./target/release/populate /opt/bin/
RUN cp ./target/release/kb_api /opt/bin/
COPY .env /opt/bin/

WORKDIR /opt/bin/
RUN /opt/bin/migration
RUN /opt/bin/populate --mode mock
CMD /opt/bin/kb_api --interface all