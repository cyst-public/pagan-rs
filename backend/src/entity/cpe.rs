//! `SeaORM` Entity. Generated by sea-orm-codegen 0.12.2

use sea_orm::entity::prelude::*;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, PartialEq, DeriveEntityModel, Eq, Serialize, Deserialize)]
#[sea_orm(table_name = "cpe")]
pub struct Model {
    #[sea_orm(primary_key)]
    pub id: i32,
    pub cpe_name: String,
    pub created: DateTime,
    pub last_modified: DateTime,
    pub service: i32,
}

#[derive(Copy, Clone, Debug, EnumIter, DeriveRelation)]
pub enum Relation {
    #[sea_orm(
        belongs_to = "super::services::Entity",
        from = "Column::Service",
        to = "super::services::Column::Id",
        on_update = "Cascade",
        on_delete = "Cascade"
    )]
    Services,
}

impl Related<super::services::Entity> for Entity {
    fn to() -> RelationDef {
        Relation::Services.def()
    }
}

impl ActiveModelBehavior for ActiveModel {}
