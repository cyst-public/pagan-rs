//! `SeaORM` Entity. Generated by sea-orm-codegen 0.12.2

use pyo3::prelude::*;
use sea_orm::entity::prelude::*;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, PartialEq, DeriveEntityModel, Eq, Serialize, Deserialize)]
#[sea_orm(table_name = "services")]
#[pyclass(name = "ServiceModel", frozen)]
pub struct Model {
    #[sea_orm(primary_key)]
    pub id: i32,
    pub name: String,
    pub version: String,
    pub vendor: String,
}

#[pymethods]
impl Model {
    #[new]
    fn new(id: i32, name: String, version: String, vendor: String) -> Self {
        Self {
            id,
            name,
            version,
            vendor,
        }
    }
}

#[derive(Copy, Clone, Debug, EnumIter, DeriveRelation)]
pub enum Relation {
    #[sea_orm(has_many = "super::applicability_matrix::Entity")]
    ApplicabilityMatrix,
    #[sea_orm(has_many = "super::cpe::Entity")]
    Cpe,
    #[sea_orm(has_many = "super::deployment_data::Entity")]
    DeploymentData,
    #[sea_orm(has_one = "super::functional_data::Entity")]
    FunctionalData,
    #[sea_orm(has_many = "super::origin_data::Entity")]
    OriginData,
    #[sea_orm(has_many = "super::service_impacts::Entity")]
    ServiceImpacts,
    #[sea_orm(has_many = "super::service_tags::Entity")]
    ServiceTags,
}

impl Related<super::applicability_matrix::Entity> for Entity {
    fn to() -> RelationDef {
        Relation::ApplicabilityMatrix.def()
    }
}

impl Related<super::cpe::Entity> for Entity {
    fn to() -> RelationDef {
        Relation::Cpe.def()
    }
}

impl Related<super::deployment_data::Entity> for Entity {
    fn to() -> RelationDef {
        Relation::DeploymentData.def()
    }
}

impl Related<super::functional_data::Entity> for Entity {
    fn to() -> RelationDef {
        Relation::FunctionalData.def()
    }
}

impl Related<super::origin_data::Entity> for Entity {
    fn to() -> RelationDef {
        Relation::OriginData.def()
    }
}

impl Related<super::service_impacts::Entity> for Entity {
    fn to() -> RelationDef {
        Relation::ServiceImpacts.def()
    }
}

impl Related<super::service_tags::Entity> for Entity {
    fn to() -> RelationDef {
        Relation::ServiceTags.def()
    }
}

impl ActiveModelBehavior for ActiveModel {}
