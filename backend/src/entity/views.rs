use crate::entity::sea_orm_active_enums::*;
use pyo3::prelude::*;
use sea_orm::*;
use serde;
use serde_json::value::Value as Json;

#[derive(FromQueryResult, serde::Deserialize, serde::Serialize, Debug)]
#[pyclass]
pub struct ActionApplicability {
    pub service_id: i32,
    pub action_id: i32,
    pub enabler_id: i32,
    pub action_description: Json,
    pub enabler_description: Json,
    pub enabler_type: EnablerType,
    pub guard: ActionGuard,
}

#[derive(FromQueryResult, serde::Deserialize, serde::Serialize, Debug)]
#[pyclass]
pub struct PrimaryKeyView {
    id: i32,
}

impl PrimaryKeyView {
    pub fn get(&self) -> i32 {
        self.id
    }
}
