use pyo3;
use pyo3::exceptions::PyRuntimeError;
use sea_orm;
use serde_json;
use std;
use std::fmt::{Display, Formatter};
use strum;

#[derive(Debug)]
pub enum BackendError {
    StrumParse(strum::ParseError),
    SeaOrmDbError(sea_orm::error::DbErr),
    SerdeJsonError(serde_json::error::Error),
    StopIteration,
    InvalidBits(flagset::InvalidBits),
    MalformedPredicate,
    InvalidPortNumber,
    NoSuchPrimaryKey(String),
    AutomatonBuildingError(String),
    TopologyError(String),
}

impl From<strum::ParseError> for BackendError {
    fn from(value: strum::ParseError) -> Self {
        BackendError::StrumParse(value)
    }
}

impl From<sea_orm::error::DbErr> for BackendError {
    fn from(value: sea_orm::error::DbErr) -> Self {
        BackendError::SeaOrmDbError(value)
    }
}

impl From<serde_json::error::Error> for BackendError {
    fn from(value: serde_json::error::Error) -> Self {
        BackendError::SerdeJsonError(value)
    }
}

impl From<BackendError> for pyo3::prelude::PyErr {
    fn from(value: BackendError) -> Self {
        PyRuntimeError::new_err(format!("{:?}", value))
    }
}

impl From<flagset::InvalidBits> for BackendError {
    fn from(value: flagset::InvalidBits) -> Self {
        BackendError::InvalidBits(value)
    }
}

impl Display for BackendError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            BackendError::StrumParse(e) => {
                write!(f, "{}", e)
            }
            BackendError::SeaOrmDbError(e) => {
                write!(f, "{}", e)
            }
            BackendError::SerdeJsonError(e) => {
                write!(f, "{}", e)
            }
            BackendError::StopIteration => {
                write!(f, "Stop the Iteration")
            }
            BackendError::InvalidBits(e) => {
                write!(f, "{}", e)
            }
            BackendError::MalformedPredicate => {
                write!(f, "The predicate is invalid")
            }
            BackendError::InvalidPortNumber => {
                write!(f, "The port number is invalid")
            }
            BackendError::NoSuchPrimaryKey(reason) => {
                write!(f, "Primary key not present; {}", reason)
            }
            BackendError::AutomatonBuildingError(reason) => {
                write!(f, "Automaton building failed; {}", reason)
            }
            BackendError::TopologyError(reason) => {
                write!(f, "Topology inconsistency; {}", reason)
            }
        }
    }
}

impl std::error::Error for BackendError {}
