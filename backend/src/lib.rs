#![feature(async_closure)]
#![feature(coroutines)]
#![feature(coroutine_trait)]
#![feature(iter_from_coroutine)]

#[macro_use]

pub mod entity;
pub mod error;
pub mod mutations;
pub mod primitives;
pub mod queries;
pub mod stores;

// #[cfg(crate_type = "cdylib")]
mod pymod;
