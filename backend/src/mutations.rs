use crate::entity::prelude::*;
use sea_orm::*;

pub struct Mutations {}

impl Mutations {
    pub async fn save_service(
        data: ServiceModel,
        db: &DatabaseConnection,
    ) -> Result<ServiceAcM, DbErr> {
        ServiceAcM {
            name: Set(data.name),
            vendor: Set(data.vendor),
            version: Set(data.version),
            ..Default::default()
        }
        .save(db)
        .await
    }

    pub async fn save_cpe(data: CpeModel, db: &DatabaseConnection) -> Result<CpeAcM, DbErr> {
        CpeAcM {
            cpe_name: Set(data.cpe_name),
            created: Set(data.created),
            last_modified: Set(data.last_modified),
            service: Set(data.service),
            ..Default::default()
        }
        .save(db)
        .await
    }

    pub async fn insert_multiple_cpes<I>(
        data_collection: I,
        db: &DatabaseConnection,
    ) -> Result<(), DbErr>
    where
        I: IntoIterator<Item = CpeModel>,
    {
        CpeEnt::insert_many(
            data_collection
                .into_iter()
                .map(move |data: CpeModel| CpeAcM {
                    cpe_name: Set(data.cpe_name),
                    created: Set(data.created),
                    last_modified: Set(data.last_modified),
                    service: Set(data.service),
                    ..Default::default()
                }),
        )
        .exec(db)
        .await?;
        Ok(())
    }

    pub async fn save_functional_data(
        data: FunctionalDataModel,
        db: &DatabaseConnection,
    ) -> Result<FunctionalDataAcM, DbErr> {
        FunctionalDataAcM {
            auto_elevation: Set(data.auto_elevation),
            executable_access: Set(data.executable_access),
            ports: Set(data.ports),
            service: Set(data.service),
            target_software: Set(data.target_software),
            ..Default::default()
        }
        .save(db)
        .await
    }

    pub async fn insert_multiple_functional_data<I>(
        data_collection: I,
        db: &DatabaseConnection,
    ) -> Result<(), DbErr>
    where
        I: IntoIterator<Item = FunctionalDataModel>,
    {
        FunctionalDataEnt::insert_many(data_collection.into_iter().map(
            move |data: FunctionalDataModel| FunctionalDataAcM {
                auto_elevation: Set(data.auto_elevation),
                executable_access: Set(data.executable_access),
                ports: Set(data.ports),
                service: Set(data.service),
                target_software: Set(data.target_software),
                ..Default::default()
            },
        ))
        .exec(db)
        .await?;
        Ok(())
    }

    pub async fn save_origin_data(
        data: OriginDataModel,
        db: &DatabaseConnection,
    ) -> Result<OriginDataAcM, DbErr> {
        OriginDataAcM {
            description: Set(data.description),
            change_log: Set(data.change_log),
            source: Set(data.source),
            service: Set(data.service),
            ..Default::default()
        }
        .save(db)
        .await
    }

    pub async fn insert_multiple_origin_data<I>(
        data_collection: I,
        db: &DatabaseConnection,
    ) -> Result<(), DbErr>
    where
        I: IntoIterator<Item = OriginDataModel>,
    {
        OriginDataEnt::insert_many(data_collection.into_iter().map(
            move |data: OriginDataModel| OriginDataAcM {
                description: Set(data.description),
                change_log: Set(data.change_log),
                source: Set(data.source),
                service: Set(data.service),
                ..Default::default()
            },
        ))
        .exec(db)
        .await?;
        Ok(())
    }

    pub async fn save_service_tag(
        data: ServiceTagModel,
        db: &DatabaseConnection,
    ) -> Result<ServiceTagAcM, DbErr> {
        ServiceTagAcM {
            service_id: Set(data.service_id),
            tag: Set(data.tag),
        }
        .save(db)
        .await
    }

    pub async fn insert_multiple_tags<I>(
        data_collection: I,
        db: &DatabaseConnection,
    ) -> Result<(), DbErr>
    where
        I: IntoIterator<Item = ServiceTagModel>,
    {
        ServiceTagEnt::insert_many(data_collection.into_iter().map(
            move |data: ServiceTagModel| ServiceTagAcM {
                service_id: Set(data.service_id),
                tag: Set(data.tag),
            },
        ))
        .exec(db)
        .await?;
        Ok(())
    }

    pub async fn save_service_impact(
        data: ServiceImpactModel,
        db: &DatabaseConnection,
    ) -> Result<ServiceImpactAcM, DbErr> {
        ServiceImpactAcM {
            service_id: Set(data.service_id),
            impact: Set(data.impact),
        }
        .save(db)
        .await
    }

    pub async fn insert_multiple_impacts<I>(
        data_collection: I,
        db: &DatabaseConnection,
    ) -> Result<(), DbErr>
    where
        I: IntoIterator<Item = ServiceImpactModel>,
    {
        ServiceImpactEnt::insert_many(data_collection.into_iter().map(
            move |data: ServiceImpactModel| ServiceImpactAcM {
                service_id: Set(data.service_id),
                impact: Set(data.impact),
            },
        ))
        .exec(db)
        .await?;
        Ok(())
    }

    pub async fn insert_action(
        data: ActionModel,
        db: &DatabaseConnection,
    ) -> Result<ActionAcM, DbErr> {
        ActionAcM {
            id: Set(data.id),
            description: Set(data.description),
        }
        .save(db)
        .await
    }

    pub async fn insert_multiple_actions<I>(
        data_collection: I,
        db: &DatabaseConnection,
    ) -> Result<(), DbErr>
    where
        I: IntoIterator<Item = ActionModel>,
    {
        ActionEnt::insert_many(data_collection.into_iter().map(move |data: ActionModel| {
            ActionAcM {
                id: Set(data.id),
                description: Set(data.description),
            }
        }))
        .exec(db)
        .await?;
        Ok(())
    }

    pub async fn insert_enabler(
        data: EnablerModel,
        db: &DatabaseConnection,
    ) -> Result<EnablerAcM, DbErr> {
        EnablerAcM {
            r#type: Set(data.r#type),
            details: Set(data.details),
            main_cpe: Set(data.main_cpe),
            config: Set(data.config),
            ..Default::default()
        }
        .save(db)
        .await
    }

    pub async fn insert_multiple_enablers<I>(
        data_collection: I,
        db: &DatabaseConnection,
    ) -> Result<(), DbErr>
    where
        I: IntoIterator<Item = EnablerModel>,
    {
        EnablerEnt::insert_many(data_collection.into_iter().map(move |data: EnablerModel| {
            EnablerAcM {
                r#type: Set(data.r#type),
                details: Set(data.details),
                main_cpe: Set(data.main_cpe),
                config: Set(data.config),
                ..Default::default()
            }
        }))
        .exec(db)
        .await?;
        Ok(())
    }
}
