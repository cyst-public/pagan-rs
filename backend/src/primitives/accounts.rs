use crate::error::BackendError;
use crate::no_wildcards_check;
use crate::primitives::capabilities::Capability;
use crate::primitives::credentials::Credential;
use crate::primitives::device::Device;
use crate::primitives::person::Person;
use crate::primitives::shared::traits::{ComplexStrength, PrimitiveStrength};
use crate::primitives::shared::types::DeviceHandle;
use macros::{compare_optional_weakref_attributes_for_eq, hash_optional_weakref_attribute};
use pyo3::pyclass;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};
use std::sync::{Arc, Weak};

#[pyclass]
#[derive(Debug)]
pub struct Account {
    owner: Option<Weak<Person>>,
    nickname: String,
    credentials: Vec<Arc<Credential>>,
    capabilities: HashMap<DeviceHandle, Vec<Arc<Capability>>>,
    domain_account: bool,
    valid: bool,
    devices: HashMap<DeviceHandle, Weak<Device>>,
}

impl PartialEq for Account {
    fn eq(&self, other: &Self) -> bool {
        if self.domain_account != other.domain_account {
            return false;
        }
        if self.nickname == other.nickname {
            return false;
        }
        if self.credentials != other.credentials {
            return false;
        }
        if self.capabilities != other.capabilities {
            return false;
        }
        if self.valid != other.valid {
            return false;
        }
        compare_optional_weakref_attributes_for_eq!(self.owner, other.owner);

        return true;
    }
}

impl Eq for Account {}

impl Hash for Account {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.nickname.hash(state);
        self.domain_account.hash(state);
        self.valid.hash(state);
        self.credentials
            .iter()
            .for_each(|item| item.as_ref().hash(state));
        self.capabilities.iter().for_each(|item| item.hash(state));
        hash_optional_weakref_attribute!(
            self.owner,
            Person,
            state,
            "Invalid weak reference, an account can not loose its owner."
        );
    }
}

impl Account {
    pub fn new(
        owner: Option<Weak<Person>>,
        nickname: String,
        credentials: Vec<Arc<Credential>>,
        capabilities: HashMap<DeviceHandle, Vec<Arc<Capability>>>,
        domain_account: bool,
        devices: Option<HashMap<DeviceHandle, Weak<Device>>>,
    ) -> Result<Self, BackendError> {
        no_wildcards_check!(
            credentials,
            "Adding wildcard credentials to an account is invalid."
        );
        Ok(Self {
            owner,
            nickname,
            credentials,
            capabilities,
            domain_account,
            valid: true,
            devices: devices.unwrap_or(HashMap::new()),
        })
    }

    pub fn owner(&self) -> &Option<Weak<Person>> {
        &self.owner
    }

    pub fn nickname(&self) -> &str {
        &self.nickname
    }

    pub fn credentials(&self) -> &Vec<Arc<Credential>> {
        &self.credentials
    }

    pub fn capabilities(&self) -> &HashMap<DeviceHandle, Vec<Arc<Capability>>> {
        &self.capabilities
    }

    pub fn domain_account(&self) -> bool {
        self.domain_account
    }

    pub fn add_credential(&mut self, credential: Arc<Credential>) -> Result<(), BackendError> {
        match credential.as_ref() {
            Credential::Wildcard(_) => Err(BackendError::TopologyError(
                "Adding wildcard credentials to accounts is invalid.".to_string(),
            )),
            Credential::Instance(_) => {
                self.credentials.push(credential);
                Ok(())
            }
        }
    }

    pub fn add_capability(&mut self, capability: Capability) {
        match self.capabilities.get_mut(&capability.device()) {
            None => {
                self.capabilities
                    .insert(capability.device(), vec![Arc::new(capability)]);
            }
            Some(list) => list.push(Arc::new(capability)),
        };
    }

    pub fn get_associated_credentials(&self) -> Vec<Weak<Credential>> {
        match &self.owner {
            None => {
                vec![]
            }
            Some(person_weak_ref) => match person_weak_ref.upgrade() {
                None => {
                    vec![]
                }
                Some(person) => std::iter::from_coroutine(person.credentials())
                    .map(|cred| Arc::downgrade(&cred))
                    .collect::<Vec<_>>(),
            },
        }
    }

    pub fn invalidate(&mut self) {
        self.valid = false;
    }

    pub fn add_device(&mut self, handle: DeviceHandle, device: &Arc<Device>) {
        self.devices.insert(handle, Arc::downgrade(device));
    }
}

impl ComplexStrength for Account {
    fn strength(&self, device: Option<DeviceHandle>) -> u64 {
        match &device {
            None => {
                let mut res = 0u64;
                for (id, device) in self.devices.iter() {
                    if let Some(device_strong_ref) = device.upgrade() {
                        if let Some(caps) = self.capabilities.get(id) {
                            res += caps.iter().map(|cap| cap.strength()).sum::<u64>()
                                * device_strong_ref.strength();
                        }
                    } else {
                        continue;
                    }
                }
                res
            }
            Some(dev) => match self.capabilities.get(dev) {
                None => 0u64,
                Some(caps) => {
                    if let Some(device_weak_ref) = self.devices.get(dev) {
                        if let Some(device_strong_ref) = device_weak_ref.upgrade() {
                            device_strong_ref.strength()
                                * caps.iter().map(|cap| cap.strength()).sum::<u64>()
                        } else {
                            0
                        }
                    } else {
                        0
                    }
                }
            },
        }
    }
}
