use crate::entity::sea_orm_active_enums::EnablerImpactType;
use crate::error::BackendError;
use crate::primitives::capabilities::{AttackerCapability, CapabilityTemplate};
use crate::primitives::credentials::Credential;
use crate::primitives::enablers::{EnablerLocality, EnablerTemplate};
use crate::primitives::predicate::predicate_builders::Builder;
use crate::primitives::predicate::{predicate_builders, Predicate};
use crate::primitives::shared::artifacts::Token;
use crate::primitives::shared::enums::{
    CapabilitySpecifier, CapabilityType, CredentialOrigin, CredentialType, LimitType,
    PrivilegeLevel, PrivilegeOrigin, TokenType,
};
use crate::primitives::shared::traits::PrimitiveStrength;
use crate::primitives::shared::types::{ActionHandle, DeviceHandle, SchemeHandle};
use crate::primitives::shared::utils::Counter;
use flagset::FlagSet;
use once_cell::sync::Lazy;
use std::collections::HashMap;
use std::sync::Arc;

macro_rules! filter_for_device_and_clone_arc {
    ($collection: expr, $device: ident) => {
        $collection
            .iter()
            .filter_map(|item| {
                if item.is_for_device($device) {
                    Some(Arc::clone(item))
                } else {
                    None
                }
            })
            .collect::<Vec<_>>()
    };
}

#[derive(Debug)]
pub struct ArtifactSatchel {
    tokens: Vec<Arc<Token>>,
    capabilities: Vec<Arc<AttackerCapability>>,
    credentials: Vec<Arc<Credential>>,
}
impl ArtifactSatchel {
    pub fn new(
        tokens: &Vec<Arc<Token>>,
        capabilities: &Vec<Arc<AttackerCapability>>,
        credentials: &Vec<Arc<Credential>>,
        device: &DeviceHandle,
    ) -> Self {
        Self {
            tokens: filter_for_device_and_clone_arc!(tokens, device),
            capabilities: filter_for_device_and_clone_arc!(capabilities, device),
            credentials: filter_for_device_and_clone_arc!(credentials, device),
        }
    }

    pub fn tokens(&self) -> &Vec<Arc<Token>> {
        &self.tokens
    }
    pub fn capabilities(&self) -> &Vec<Arc<AttackerCapability>> {
        &self.capabilities
    }
    pub fn credentials(&self) -> &Vec<Arc<Credential>> {
        &self.credentials
    }
}

// impl ArtifactSatchel {
//     pub fn add_tokens(&mut self, new_tokens: &[Token]) {
//         self.tokens
//             .extend(new_tokens.iter().map(|item| Arc::new(item.to_owned())));
//     }
//
//     pub fn add_capabilities(&mut self, new_caps: &[AttackerCapability]) {
//         self.capabilities
//             .extend(new_caps.iter().map(|item| Arc::new(item.to_owned())));
//     }
//
//     pub fn add_credentials(&mut self, new_creds: &[Credential]) {
//         self.credentials
//             .extend(new_creds.iter().map(|item| Arc::new(item.to_owned())));
//     }
// }
//
// impl Clone for ArtifactSatchel {
//     fn clone(&self) -> Self {
//         Self {
//             tokens: self.tokens.iter().map(Arc::clone).collect::<Vec<_>>(),
//             capabilities: self.capabilities.iter().map(Arc::clone).collect::<Vec<_>>(),
//             credentials: self.credentials.iter().map(Arc::clone).collect::<Vec<_>>(),
//         }
//     }
// }

impl ArtifactSatchel {
    pub fn compute_best_capabilities(
        &self,
        action_id: &ActionHandle,
        scheme_id: &SchemeHandle,
    ) -> Result<(Option<usize>, Vec<Arc<AttackerCapability>>), BackendError> {
        let scheme = ADVERSARY_ACTION_MAP
            .get(action_id)
            .ok_or(BackendError::NoSuchPrimaryKey(
                "Invalid action id.".to_string(),
            ))?
            .uses
            .get(scheme_id.to_owned())
            .ok_or(BackendError::NoSuchPrimaryKey(
                "Invalid usage id.".to_string(),
            ))?;

        let filtered_caps_by_user = {
            let mut temp_map: HashMap<usize, Vec<Arc<AttackerCapability>>> = HashMap::new();
            self.capabilities
                .iter()
                .filter(|a_cap| scheme.capabilities_required.atomic_is_present(a_cap))
                .for_each(|a_cap| match temp_map.get_mut(a_cap.user()) {
                    None => {
                        temp_map.insert(a_cap.user().to_owned(), vec![Arc::clone(a_cap)]);
                    }
                    Some(vec) => vec.push(Arc::clone(a_cap)),
                });
            temp_map.retain(|_key, value| {
                scheme
                    .capabilities_required
                    .eval(value.iter().map(Arc::as_ref).collect::<Vec<_>>().as_slice())
                    .unwrap()
            }); // does not panic with capability predicate
            temp_map
        };
        let best_option = {
            let mut max_cap_strength = 0f64;
            let mut best_batch_id = None;
            for (key, caps) in filtered_caps_by_user.iter() {
                let avg = caps.iter().map(|a_cap| a_cap.strength()).sum::<u64>() as f64
                    / caps.len() as f64;
                if avg > max_cap_strength {
                    max_cap_strength = avg;
                    best_batch_id = Option::from(key.to_owned())
                }
            }
            best_batch_id
        };
        match &best_option {
            None => Ok((None, vec![])),
            Some(val) => Ok((
                best_option,
                filtered_caps_by_user
                    .get(val)
                    .ok_or(BackendError::NoSuchPrimaryKey(
                        "Invalid user idx".to_string(),
                    ))?
                    .to_owned(),
            )),
        }
    }
}

// #[pymethods]
// impl ArtifactSatchel {
//     pub fn extend_with(
//         &mut self,
//         tokens: &PyList,
//         capabilities: &PyList,
//         credentials: &PyList,
//     ) -> PyResult<()> {
//         for token in tokens.iter() {
//             self.tokens.push(token.extract()?);
//         }
//         for cap in capabilities {
//             self.capabilities.push(cap.extract()?)
//         }
//         for cred in credentials {
//             self.credentials.push(cred.extract()?)
//         }
//         Ok(())
//     }
// }

#[derive(serde::Serialize)]
pub struct UsageScheme {
    pub given: FlagSet<TokenType>,
    pub grant: FlagSet<TokenType>,
    pub capabilities_required: Predicate<AttackerCapability>,
    pub capabilities_provided: Vec<CapabilityTemplate>,
    pub enabler_required: Option<EnablerTemplate>,
    pub except_if: Predicate<TokenType>,
    pub privilege_origin: PrivilegeOrigin,
    pub capability_specifier: CapabilitySpecifier,
    pub credential_origin: CredentialOrigin,
    pub credential_type: CredentialType,
}

#[derive(serde::Serialize)]
pub struct ActionDescription {
    pub name: String,
    pub aliases: Vec<String>,
    pub remote: bool,
    pub uses: Vec<UsageScheme>,
    pub notes: Option<String>,
}

impl ActionDescription {
    pub fn uses(&self) -> &Vec<UsageScheme> {
        &self.uses()
    }
}

pub static ADVERSARY_ACTION_MAP: Lazy<HashMap<ActionHandle, ActionDescription>> = Lazy::new(|| {
    let mut map = HashMap::new();
    let mut counter = Counter::default();

    /* <InitialCompromise> */
    map.insert(
        counter.next(),
        ActionDescription {
            name: "Discover accessible devices and services".to_string(),
            aliases: vec![
                "T1595".to_string(),
                "T1592".to_string(),
                "T1590".to_string(),
            ],
            remote: false,
            uses: vec![UsageScheme {
                given: TokenType::Session | TokenType::Capability,
                grant: FlagSet::from(TokenType::AccessViaNetwork),
                capabilities_required: predicate_builders::All::build_from([CapabilityTemplate {
                    to: CapabilityType::CodeExecution,
                    privilege_level: PrivilegeLevel::Service,
                    limit: LimitType::Minimum,
                    device: None,
                }]),
                capabilities_provided: vec![],
                enabler_required: None,
                except_if: predicate_builders::Empty::build_from(()),
                privilege_origin: PrivilegeOrigin::None,
                capability_specifier: CapabilitySpecifier::None,
                credential_origin: CredentialOrigin::None,
                credential_type: CredentialType::None,
            }],
            notes: None,
        },
    );
    map.insert(
        counter.next(),
        ActionDescription {
            name: "Smuggle infected media".to_string(),
            aliases: vec![],
            remote: false,
            uses: vec![UsageScheme {
                given: FlagSet::from(TokenType::PhysicalAccess),
                grant: FlagSet::from(TokenType::InfectedRemovableMedia),
                capabilities_required: predicate_builders::Empty::build_from(()),
                capabilities_provided: vec![],
                enabler_required: None,
                except_if: predicate_builders::All::build_from([TokenType::AccessInternalNetwork]),
                privilege_origin: PrivilegeOrigin::None,
                capability_specifier: CapabilitySpecifier::None,
                credential_origin: CredentialOrigin::None,
                credential_type: CredentialType::None,
            }],
            notes: None,
        },
    );

    map.insert(
        counter.next(),
        ActionDescription {
            name: "Exploit open application".to_string(),
            aliases: vec!["T1190".to_string()],
            remote: true,
            uses: vec![
                UsageScheme {
                    given: FlagSet::from(TokenType::AccessViaNetwork),
                    grant: TokenType::Session | TokenType::CapabilityAll,
                    capabilities_required: predicate_builders::Empty::build_from(()),
                    capabilities_provided: vec![],
                    enabler_required: Some(EnablerTemplate::new(
                        EnablerLocality::Remote,
                        predicate_builders::All::build_from([
                            EnablerImpactType::ArbitraryCodeExecution,
                        ]),
                    )),
                    except_if: predicate_builders::Empty::build_from(()),
                    privilege_origin: PrivilegeOrigin::Service,
                    capability_specifier: CapabilitySpecifier::None,
                    credential_origin: CredentialOrigin::None,
                    credential_type: CredentialType::None,
                },
                UsageScheme {
                    given: FlagSet::from(TokenType::AccessViaNetwork),
                    grant: FlagSet::from(TokenType::Capability),
                    capabilities_required: predicate_builders::Empty::build_from(()),
                    capabilities_provided: vec![],
                    enabler_required: Some(EnablerTemplate::new(
                        EnablerLocality::Remote,
                        predicate_builders::All::build_from([
                            predicate_builders::Not::build_from(
                                EnablerImpactType::ArbitraryCodeExecution,
                            ),
                            predicate_builders::NotJust::build_from([EnablerImpactType::Allow]),
                        ]),
                    )),
                    except_if: Predicate::Empty,
                    privilege_origin: PrivilegeOrigin::Service,
                    capability_specifier: CapabilitySpecifier::Enabler,
                    credential_origin: CredentialOrigin::None,
                    credential_type: CredentialType::None,
                },
            ],
            notes: None,
        },
    );

    map.insert(
        counter.next(),
        ActionDescription {
            name: "Exploit authed application".to_string(),
            aliases: vec!["T1190".to_string()],
            remote: true,
            uses: vec![
                UsageScheme {
                    given: TokenType::AccessViaNetwork | TokenType::Credential,
                    grant: TokenType::Session | TokenType::CapabilityAll,
                    capabilities_required: predicate_builders::Empty::build_from(()),
                    capabilities_provided: vec![],
                    enabler_required: Some(EnablerTemplate::new(
                        EnablerLocality::Remote,
                        predicate_builders::All::build_from([
                            EnablerImpactType::ArbitraryCodeExecution,
                        ]),
                    )),
                    except_if: predicate_builders::Empty::build_from(()),
                    privilege_origin: PrivilegeOrigin::Service,
                    capability_specifier: CapabilitySpecifier::None,
                    credential_origin: CredentialOrigin::None,
                    credential_type: CredentialType::None,
                },
                UsageScheme {
                    given: TokenType::AccessViaNetwork | TokenType::Credential,
                    grant: FlagSet::from(TokenType::Capability),
                    capabilities_required: predicate_builders::Empty::build_from(()),
                    capabilities_provided: vec![],
                    enabler_required: Some(EnablerTemplate::new(
                        EnablerLocality::Remote,
                        predicate_builders::All::build_from([
                            predicate_builders::Not::build_from(
                                EnablerImpactType::ArbitraryCodeExecution,
                            ),
                            predicate_builders::NotJust::build_from([EnablerImpactType::Allow]),
                        ]),
                    )),
                    except_if: Predicate::Empty,
                    privilege_origin: PrivilegeOrigin::Service,
                    capability_specifier: CapabilitySpecifier::Enabler,
                    credential_origin: CredentialOrigin::None,
                    credential_type: CredentialType::None,
                },
            ],
            notes: None,
        },
    );

    map.insert(
        counter.next(),
        ActionDescription {
            name: "Drive-by compromise".to_string(),
            aliases: vec!["T1190".to_string()],
            remote: true,
            uses: vec![
                UsageScheme {
                    given: FlagSet::from(TokenType::None),
                    grant: TokenType::Session | TokenType::CapabilityAll,
                    capabilities_required: predicate_builders::Empty::build_from(()),
                    capabilities_provided: vec![],
                    enabler_required: Some(EnablerTemplate::new(
                        EnablerLocality::Remote,
                        predicate_builders::All::build_from([
                            EnablerImpactType::ArbitraryCodeExecution,
                        ]),
                    )),
                    except_if: predicate_builders::Any::build_from([
                        TokenType::AccessInternalNetwork,
                    ]),
                    privilege_origin: PrivilegeOrigin::Service,
                    capability_specifier: CapabilitySpecifier::None,
                    credential_origin: CredentialOrigin::None,
                    credential_type: CredentialType::None,
                },
                UsageScheme {
                    given: FlagSet::from(TokenType::None),
                    grant: FlagSet::from(TokenType::Capability),
                    capabilities_required: predicate_builders::Empty::build_from(()),
                    capabilities_provided: vec![],
                    enabler_required: Some(EnablerTemplate::new(
                        EnablerLocality::Remote,
                        predicate_builders::All::build_from([
                            predicate_builders::Not::build_from(
                                EnablerImpactType::ArbitraryCodeExecution,
                            ),
                            predicate_builders::NotJust::build_from([EnablerImpactType::Allow]),
                        ]),
                    )),
                    except_if: predicate_builders::Any::build_from([
                        TokenType::AccessInternalNetwork,
                    ]),
                    privilege_origin: PrivilegeOrigin::Service,
                    capability_specifier: CapabilitySpecifier::Enabler,
                    credential_origin: CredentialOrigin::None,
                    credential_type: CredentialType::None,
                },
            ],
            notes: None,
        },
    );

    map.insert(
        counter.next(),
        ActionDescription {
            name: "Malware phishing".to_string(),
            aliases: vec!["T1566".to_string()],
            remote: true,
            uses: vec![
                UsageScheme {
                    given: FlagSet::from(TokenType::ExternalPhishingMail),
                    grant: TokenType::Session | TokenType::CapabilityAll,
                    capabilities_required: predicate_builders::Empty::build_from(()),
                    capabilities_provided: vec![],
                    enabler_required: None,
                    except_if: predicate_builders::Any::build_from([
                        TokenType::AccessInternalNetwork,
                        TokenType::AccessDmzSegment,
                    ]),
                    privilege_origin: PrivilegeOrigin::RandomUser,
                    capability_specifier: CapabilitySpecifier::None,
                    credential_origin: CredentialOrigin::None,
                    credential_type: CredentialType::None,
                },
                UsageScheme {
                    given: FlagSet::from(TokenType::InternalPhishingMail),
                    grant: TokenType::Session | TokenType::CapabilityAll,
                    capabilities_required: predicate_builders::Empty::build_from(()),
                    capabilities_provided: vec![],
                    enabler_required: None,
                    except_if: predicate_builders::Empty::build_from(()),
                    privilege_origin: PrivilegeOrigin::RandomUser,
                    capability_specifier: CapabilitySpecifier::None,
                    credential_origin: CredentialOrigin::None,
                    credential_type: CredentialType::None,
                },
            ],
            notes: None,
        },
    );

    map.insert(
        counter.next(),
        ActionDescription {
            name: "Supply chain compromise".to_string(),
            aliases: vec![
                "T1200".to_string(),
                "T1090".to_string(),
                "T1195".to_string(),
            ],
            remote: true,
            uses: vec![UsageScheme {
                given: FlagSet::from(TokenType::PreAttackBackdoor),
                grant: TokenType::Session | TokenType::CapabilityAll,
                capabilities_required: predicate_builders::Empty::build_from(()),
                capabilities_provided: vec![],
                enabler_required: None,
                except_if: predicate_builders::Any::build_from([TokenType::AccessInternalNetwork]),
                privilege_origin: PrivilegeOrigin::Service,
                capability_specifier: CapabilitySpecifier::None,
                credential_origin: CredentialOrigin::None,
                credential_type: CredentialType::None,
            }],
            notes: None,
        },
    );

    map.insert(
        counter.next(),
        ActionDescription {
            name: "Valid accounts".to_string(),
            aliases: vec!["T1133".to_string(), "T1078".to_string()],
            remote: true,
            uses: vec![
                UsageScheme {
                    given: TokenType::AccessViaNetwork | TokenType::Credential,
                    grant: TokenType::Session | TokenType::CapabilityAll,
                    capabilities_required: predicate_builders::Empty::build_from(()),
                    capabilities_provided: vec![],
                    enabler_required: None,
                    except_if: predicate_builders::Empty::build_from(()),
                    privilege_origin: PrivilegeOrigin::Credential,
                    capability_specifier: CapabilitySpecifier::None,
                    credential_origin: CredentialOrigin::None,
                    credential_type: CredentialType::None,
                },
                UsageScheme {
                    given: TokenType::AccessViaNetwork
                        | TokenType::Credential
                        | TokenType::CredentialMfa,
                    grant: TokenType::Session | TokenType::CapabilityAll,
                    capabilities_required: predicate_builders::Empty::build_from(()),
                    capabilities_provided: vec![],
                    enabler_required: None,
                    except_if: predicate_builders::Any::build_from([TokenType::NoMfaAllowed]),
                    privilege_origin: PrivilegeOrigin::Credential,
                    capability_specifier: CapabilitySpecifier::None,
                    credential_origin: CredentialOrigin::None,
                    credential_type: CredentialType::None,
                },
                UsageScheme {
                    given: FlagSet::from(TokenType::AccessViaNetwork),
                    grant: FlagSet::from(TokenType::Capability),
                    capabilities_required: predicate_builders::Empty::build_from(()),
                    capabilities_provided: vec![],
                    enabler_required: Some(EnablerTemplate::new(
                        EnablerLocality::Remote,
                        predicate_builders::NotJust::build_from([EnablerImpactType::Allow]),
                    )),
                    except_if: predicate_builders::Empty::build_from(()),
                    privilege_origin: PrivilegeOrigin::Credential,
                    capability_specifier: CapabilitySpecifier::Enabler,
                    credential_origin: CredentialOrigin::None,
                    credential_type: CredentialType::None,
                },
            ],
            notes: None,
        },
    );
    /* </InitialCompromise> */

    /* <Persistence> */
    map.insert(
        counter.next(),
        ActionDescription {
            name: "Persistence with backdoor".to_string(),
            aliases: vec![
                "T1053".to_string(),
                "T1547".to_string(),
                "T1037".to_string(),
            ],
            remote: false,
            uses: vec![UsageScheme {
                given: TokenType::Session | TokenType::Capability,
                grant: FlagSet::from(TokenType::Capability),
                capabilities_required: predicate_builders::All::build_from([CapabilityTemplate {
                    to: CapabilityType::CodeExecution,
                    privilege_level: PrivilegeLevel::Service,
                    limit: LimitType::Minimum,
                    device: None,
                }]),
                capabilities_provided: vec![CapabilityTemplate {
                    to: CapabilityType::Persist,
                    privilege_level: PrivilegeLevel::Service,
                    limit: LimitType::Minimum,
                    device: None,
                }],
                enabler_required: None,
                except_if: predicate_builders::Empty::build_from(()),
                privilege_origin: PrivilegeOrigin::InheritUser,
                capability_specifier: CapabilitySpecifier::Action,
                credential_origin: CredentialOrigin::None,
                credential_type: CredentialType::None,
            }],
            notes: None,
        },
    );

    map.insert(
        counter.next(),
        ActionDescription {
            name: "Persistence with user".to_string(),
            aliases: vec!["T1098".to_string()],
            remote: false,
            uses: vec![UsageScheme {
                given: TokenType::Session | TokenType::Capability,
                grant: TokenType::CapabilityAll | TokenType::Credential,
                capabilities_required: predicate_builders::All::build_from([CapabilityTemplate {
                    to: CapabilityType::AddUser,
                    privilege_level: PrivilegeLevel::User,
                    limit: LimitType::Minimum,
                    device: None,
                }]),
                capabilities_provided: vec![CapabilityTemplate {
                    to: CapabilityType::Persist,
                    privilege_level: PrivilegeLevel::User,
                    limit: LimitType::Minimum,
                    device: None,
                }],
                enabler_required: None,
                except_if: predicate_builders::Empty::build_from(()),
                privilege_origin: PrivilegeOrigin::NewUser,
                capability_specifier: CapabilitySpecifier::None,
                credential_origin: CredentialOrigin::NewUser,
                credential_type: CredentialType::Os,
            }],
            notes: None,
        },
    );
    /* </Persistence> */

    /* <PrivilegeEscalation> */
    map.insert(
        counter.next(),
        ActionDescription {
            name: "Abuse Elevation Control Mechanism: Setuid and Setgid".to_string(),
            aliases: vec!["T1548.001".to_string()],
            remote: false,
            uses: vec![UsageScheme {
                given: TokenType::Session | TokenType::Capability,
                grant: FlagSet::from(TokenType::Capability),
                capabilities_required: predicate_builders::All::build_from([CapabilityTemplate {
                    to: CapabilityType::CodeExecution,
                    privilege_level: PrivilegeLevel::Service,
                    limit: LimitType::Minimum,
                    device: None,
                }]),
                capabilities_provided: vec![],
                enabler_required: Some(EnablerTemplate::new(
                    EnablerLocality::Local,
                    predicate_builders::All::build_from([EnablerImpactType::Allow]),
                )),
                except_if: predicate_builders::Empty::build_from(()),
                privilege_origin: PrivilegeOrigin::Service,
                capability_specifier: CapabilitySpecifier::Service,
                credential_origin: CredentialOrigin::None,
                credential_type: CredentialType::None,
            }],
            notes: None,
        },
    );

    map.insert(
            counter.next(),
            ActionDescription {
                name: "Abuse Elevation Control Mechanism: Bypass User Account Control".to_string(),
                aliases: vec!["T1548.002".to_string()],
                remote: false,
                uses: vec![
                    UsageScheme {
                        given: TokenType::Session | TokenType::Capability,
                        grant: FlagSet::from(TokenType::Capability),
                        capabilities_required: predicate_builders::All::build_from([
                            CapabilityTemplate {
                                to: CapabilityType::CodeExecution,
                                privilege_level: PrivilegeLevel::User,
                                limit: LimitType::Exact,
                                device: None,
                            },
                            CapabilityTemplate {
                                to: CapabilityType::UacElevate,
                                privilege_level: PrivilegeLevel::User,
                                limit: LimitType::Exact,
                                device: None,
                            }
                        ]),
                        capabilities_provided: vec![],
                        enabler_required: Some(EnablerTemplate::new(
                            EnablerLocality::Local,
                            predicate_builders::Any::build_from([
                                EnablerImpactType::ArbitraryCodeExecution,
                                EnablerImpactType::DataManipulation,
                                EnablerImpactType::MachineConfigurationTampering
                            ])
                        )),
                        except_if: predicate_builders::Empty::build_from(()),
                        privilege_origin: PrivilegeOrigin::ElevatedUser,
                        capability_specifier: CapabilitySpecifier::Enabler,
                        credential_origin: CredentialOrigin::None,
                        credential_type: CredentialType::None,
                    }
                ],
                notes: Some(r#"
                Usages are based on the https://github.com/hfiref0x/UACME techniques (click "Keys" in readme)
                * case A: application shimming requires admin priv to create shim, aka no effective privesc
                * case B: Dll Hijack -> see component hijack on filesys
                * case C: shell API - again depends on a high priv or autoElevating process to grab data/run
                * command from user controlled registry/env -> same as component hijack path
                * case D: Exposed elevated COM interface - the only standalone, implemented in this action
                "#.to_string()),
            },
        );

    map.insert(
            counter.next(),
            ActionDescription {
                name: "Abuse Elevation Control Mechanism: Sudoers".to_string(),
                aliases: vec!["T1548.003".to_string()],
                remote: false,
                uses: vec![
                    UsageScheme {
                        given: TokenType::Session | TokenType::Capability,
                        grant: FlagSet::from(TokenType::Capability),
                        capabilities_required: predicate_builders::All::build_from([
                            CapabilityTemplate {
                                to: CapabilityType::CodeExecution,
                                privilege_level: PrivilegeLevel::User,
                                limit: LimitType::Exact,
                                device: None,
                            }
                        ]),
                        capabilities_provided: vec![],
                        enabler_required: Some(
                            EnablerTemplate::new(
                                EnablerLocality::Local,
                                predicate_builders::NotJust::build_from([EnablerImpactType::Allow])
                            )
                        ),
                        except_if: predicate_builders::Empty::build_from(()),
                        privilege_origin: PrivilegeOrigin::ElevatedUser,
                        capability_specifier: CapabilitySpecifier::Enabler,
                        credential_origin: CredentialOrigin::None,
                        credential_type: CredentialType::None,
                    },
                    UsageScheme {
                        given: TokenType::Session | TokenType::Capability,
                        grant: FlagSet::from(TokenType::CapabilityAll),
                        capabilities_required: predicate_builders::All::build_from([
                            CapabilityTemplate {
                                to: CapabilityType::CodeExecution,
                                privilege_level: PrivilegeLevel::User,
                                limit: LimitType::Exact,
                                device: None,
                            }
                        ]),
                        capabilities_provided: vec![],
                        enabler_required: Some(
                            EnablerTemplate::new(
                                EnablerLocality::Local,
                                predicate_builders::All::build_from([EnablerImpactType::Allow])
                            )
                        ),
                        except_if: predicate_builders::Empty::build_from(()),
                        privilege_origin: PrivilegeOrigin::Fixed(PrivilegeLevel::LocalSystem),
                        capability_specifier: CapabilitySpecifier::None,
                        credential_origin: CredentialOrigin::None,
                        credential_type: CredentialType::None,
                    }
                ],
                notes: Some(r#"
                * Case A & B: user has sudo privileges without password prompt or tty isolation is turned off and attacker is lucky,
                * Case C: attacker can tamper with the sudoers file
                "#.to_string()),
            },
        );

    map.insert(
        counter.next(),
        ActionDescription {
            name: "Access Token Manipulation: Theft, Impersonation, PPID spoofing".to_string(),
            aliases: vec![
                "T1134.001".to_string(),
                "T1134.002".to_string(),
                "T1134.003".to_string(),
            ],
            remote: false,
            uses: vec![UsageScheme {
                given: TokenType::Session | TokenType::Capability,
                grant: FlagSet::from(TokenType::CapabilityAll),
                capabilities_required: predicate_builders::All::build_from([
                    CapabilityTemplate {
                        to: CapabilityType::CodeExecution,
                        privilege_level: PrivilegeLevel::User,
                        limit: LimitType::Minimum,
                        device: None,
                    },
                    CapabilityTemplate {
                        to: CapabilityType::SeCreateSecurityToken,
                        privilege_level: PrivilegeLevel::Administrator,
                        limit: LimitType::Exact,
                        device: None,
                    },
                    CapabilityTemplate {
                        to: CapabilityType::SeImpersonateToken,
                        privilege_level: PrivilegeLevel::Administrator,
                        limit: LimitType::Exact,
                        device: None,
                    },
                ]),
                capabilities_provided: vec![],
                enabler_required: None,
                except_if: predicate_builders::Empty::build_from(()),
                privilege_origin: PrivilegeOrigin::Fixed(PrivilegeLevel::LocalSystem),
                capability_specifier: CapabilitySpecifier::None,
                credential_origin: CredentialOrigin::None,
                credential_type: CredentialType::None,
            }],
            notes: None,
        },
    );

    map.insert(
        counter.next(),
        ActionDescription {
            name: "Privileged autoexecution".to_string(),
            aliases: vec!["T1547".to_string()],
            remote: false,
            uses: vec![
                UsageScheme {
                    given: TokenType::Session | TokenType::Capability,
                    grant: FlagSet::from(TokenType::CapabilityAll),
                    capabilities_required: predicate_builders::Any::build_from([
                        predicate_builders::Any::build_from([
                            CapabilityTemplate {
                                to: CapabilityType::CodeExecution,
                                privilege_level: PrivilegeLevel::Administrator,
                                limit: LimitType::Minimum,
                                device: None,
                            },
                            CapabilityTemplate {
                                to: CapabilityType::ConfigLocalMachine,
                                privilege_level: PrivilegeLevel::Administrator,
                                limit: LimitType::Minimum,
                                device: None,
                            },
                        ]),
                        predicate_builders::All::build_from([
                            CapabilityTemplate {
                                to: CapabilityType::AccessFileSysElevated,
                                privilege_level: PrivilegeLevel::Administrator,
                                limit: LimitType::Minimum,
                                device: None,
                            },
                            CapabilityTemplate {
                                to: CapabilityType::WriteFiles,
                                privilege_level: PrivilegeLevel::Administrator,
                                limit: LimitType::Minimum,
                                device: None,
                            },
                        ]),
                    ]),
                    capabilities_provided: vec![CapabilityTemplate {
                        to: CapabilityType::Persist,
                        privilege_level: PrivilegeLevel::LocalSystem,
                        limit: LimitType::Maximum,
                        device: None,
                    }],
                    enabler_required: None,
                    except_if: Predicate::Empty,
                    privilege_origin: PrivilegeOrigin::Fixed(PrivilegeLevel::LocalSystem),
                    capability_specifier: CapabilitySpecifier::Action,
                    credential_origin: CredentialOrigin::None,
                    credential_type: CredentialType::None,
                },
                UsageScheme {
                    given: TokenType::Session | TokenType::Capability,
                    grant: FlagSet::from(TokenType::CapabilityAll),
                    capabilities_required: predicate_builders::All::build_from([
                        CapabilityTemplate {
                            to: CapabilityType::CodeExecution,
                            privilege_level: PrivilegeLevel::Administrator,
                            limit: LimitType::Minimum,
                            device: None,
                        },
                    ]),
                    capabilities_provided: vec![CapabilityTemplate {
                        to: CapabilityType::Persist,
                        privilege_level: PrivilegeLevel::LocalSystem,
                        limit: LimitType::Maximum,
                        device: None,
                    }],
                    enabler_required: Some(EnablerTemplate::new(
                        EnablerLocality::Local,
                        predicate_builders::All::build_from([EnablerImpactType::Allow]),
                    )),
                    except_if: predicate_builders::Empty::build_from(()),
                    privilege_origin: PrivilegeOrigin::Fixed(PrivilegeLevel::LocalSystem),
                    capability_specifier: CapabilitySpecifier::Action,
                    credential_origin: CredentialOrigin::None,
                    credential_type: CredentialType::None,
                },
            ],
            notes: None,
        },
    );

    map.insert(
            counter.next(),
            ActionDescription {
                name: "Create or Modify System Process, Event Triggered Execution: Accessibility Features, Image File Execution Options Injection".to_string(),
                aliases: vec!["T1543".to_string(), "T1546.012".to_string(), "T1546.008".to_string()],
                remote: false,
                uses: vec![
                    UsageScheme {
                        given: TokenType::Session | TokenType::Capability,
                        grant: FlagSet::from(TokenType::CapabilityAll),
                        capabilities_required: predicate_builders::All::build_from([
                            CapabilityTemplate {
                                to: CapabilityType::ConfigLocalMachine,
                                privilege_level: PrivilegeLevel::Administrator,
                                limit: LimitType::Minimum,
                                device: None,
                            }
                        ]),
                        capabilities_provided: vec![
                            CapabilityTemplate {
                                to: CapabilityType::Persist,
                                privilege_level: PrivilegeLevel::Administrator,
                                limit: LimitType::Minimum,
                                device: None,
                            }
                        ],
                        enabler_required: None,
                        except_if: predicate_builders::Empty::build_from(()),
                        privilege_origin: PrivilegeOrigin::Fixed(PrivilegeLevel::LocalSystem),
                        capability_specifier: CapabilitySpecifier::Action,
                        credential_origin: CredentialOrigin::None,
                        credential_type: CredentialType::None,
                    }
                ],
                notes: None,
            },
        );

    map.insert(
        counter.next(),
        ActionDescription {
            name: "Exploitation for privilege escalation".to_string(),
            aliases: vec!["T1068".to_string()],
            remote: false,
            uses: vec![UsageScheme {
                given: TokenType::Session | TokenType::Capability,
                grant: FlagSet::from(TokenType::Capability),
                capabilities_required: predicate_builders::All::build_from([CapabilityTemplate {
                    to: CapabilityType::CodeExecution,
                    privilege_level: PrivilegeLevel::Service,
                    limit: LimitType::Minimum,
                    device: None,
                }]),
                capabilities_provided: vec![],
                enabler_required: Some(EnablerTemplate::new(
                    EnablerLocality::Local,
                    predicate_builders::Any::build_from([
                        EnablerImpactType::ArbitraryCodeExecution,
                        EnablerImpactType::MachineConfigurationTampering,
                        EnablerImpactType::DataManipulation,
                    ]),
                )),
                except_if: predicate_builders::Empty::build_from(()),
                privilege_origin: PrivilegeOrigin::Service,
                capability_specifier: CapabilitySpecifier::Enabler,
                credential_origin: CredentialOrigin::None,
                credential_type: CredentialType::None,
            }],
            notes: None,
        },
    );

    map.insert(
        counter.next(),
        ActionDescription {
            name: "Exploitation for Privilege Escalation - Bring Your Own Vulnerable Driver"
                .to_string(),
            aliases: vec!["T1068".to_string()],
            remote: false,
            uses: vec![UsageScheme {
                given: TokenType::Session | TokenType::Capability,
                grant: FlagSet::from(TokenType::Capability),
                capabilities_required: predicate_builders::All::build_from([
                    CapabilityTemplate {
                        to: CapabilityType::CodeExecution,
                        privilege_level: PrivilegeLevel::Administrator,
                        limit: LimitType::Minimum,
                        device: None,
                    },
                    CapabilityTemplate {
                        to: CapabilityType::SeLoadDriver,
                        privilege_level: PrivilegeLevel::Administrator,
                        limit: LimitType::Minimum,
                        device: None,
                    },
                ]),
                capabilities_provided: vec![CapabilityTemplate {
                    to: CapabilityType::Persist,
                    privilege_level: PrivilegeLevel::LocalSystem,
                    limit: LimitType::Maximum,
                    device: None,
                }],
                enabler_required: Some(EnablerTemplate::new(
                    EnablerLocality::Local,
                    predicate_builders::Any::build_from([
                        EnablerImpactType::ArbitraryCodeExecution,
                        EnablerImpactType::MachineConfigurationTampering,
                        EnablerImpactType::DataManipulation,
                    ]),
                )),
                except_if: predicate_builders::Empty::build_from(()),
                privilege_origin: PrivilegeOrigin::Fixed(PrivilegeLevel::LocalSystem),
                capability_specifier: CapabilitySpecifier::Enabler,
                credential_origin: CredentialOrigin::None,
                credential_type: CredentialType::None,
            }],
            notes: None,
        },
    );

    map.insert(
        counter.next(),
        ActionDescription {
            name: "Process Injection".to_string(),
            aliases: vec!["T1055".to_string(), "CAPEC-640".to_string()],
            remote: false,
            uses: vec![UsageScheme {
                given: TokenType::Session | TokenType::Capability,
                grant: FlagSet::from(TokenType::CapabilityAll),
                capabilities_required: predicate_builders::All::build_from([
                    CapabilityTemplate {
                        to: CapabilityType::CodeExecution,
                        privilege_level: PrivilegeLevel::User,
                        limit: LimitType::Minimum,
                        device: None,
                    },
                    CapabilityTemplate {
                        to: CapabilityType::SeDebug,
                        privilege_level: PrivilegeLevel::Administrator,
                        limit: LimitType::Minimum,
                        device: None,
                    },
                ]),
                capabilities_provided: vec![],
                enabler_required: None,
                except_if: predicate_builders::Empty::build_from(()),
                privilege_origin: PrivilegeOrigin::Service,
                capability_specifier: CapabilitySpecifier::None,
                credential_origin: CredentialOrigin::None,
                credential_type: CredentialType::None,
            }],
            notes: None,
        },
    );

    map.insert(
            counter.next(),
            ActionDescription {
                name: "Hijacking Components (Dlls, executables) on the filesystem, Unquoted component paths".to_string(),
                aliases: vec!["T1574.001".to_string(), "T1574.002".to_string(), "T1574.008".to_string(), "T1574.009".to_string(), "T1574.005".to_string()],
                remote: false,
                uses: vec![
                    UsageScheme {
                        given: TokenType::Session | TokenType::Capability,
                        grant: FlagSet::from(TokenType::CapabilityAll),
                        capabilities_required: predicate_builders::All::build_from([
                            CapabilityTemplate {
                                to: CapabilityType::AccessFileSysElevated,
                                privilege_level: PrivilegeLevel::Administrator,
                                limit: LimitType::Minimum,
                                device: None,
                            },
                            CapabilityTemplate {
                                to: CapabilityType::WriteFiles,
                                privilege_level: PrivilegeLevel::Administrator,
                                limit: LimitType::Minimum,
                                device: None,
                            }
                        ]),
                        capabilities_provided: vec![CapabilityTemplate {
                            to: CapabilityType::Persist,
                            privilege_level: PrivilegeLevel::Service,
                            limit: LimitType::Minimum,
                            device: None,
                        }],
                        enabler_required: None,
                        except_if: predicate_builders::Empty::build_from(()),
                        privilege_origin: PrivilegeOrigin::Fixed(PrivilegeLevel::LocalSystem),
                        capability_specifier: CapabilitySpecifier::Action,
                        credential_origin: CredentialOrigin::None,
                        credential_type: CredentialType::None,
                    },
                    UsageScheme {
                        given: TokenType::Session | TokenType::Capability,
                        grant: FlagSet::from(TokenType::CapabilityAll),
                        capabilities_required: predicate_builders::All::build_from([
                            CapabilityTemplate {
                                to: CapabilityType::AccessFileSysGeneric,
                                privilege_level: PrivilegeLevel::User,
                                limit: LimitType::Minimum,
                                device: None,
                            },
                            CapabilityTemplate {
                                to: CapabilityType::WriteFiles,
                                privilege_level: PrivilegeLevel::User,
                                limit: LimitType::Minimum,
                                device: None,
                            }
                        ]),
                        capabilities_provided: vec![CapabilityTemplate {
                            to: CapabilityType::Persist,
                            privilege_level: PrivilegeLevel::Service,
                            limit: LimitType::Minimum,
                            device: None,
                        }],
                        enabler_required: Some(
                            EnablerTemplate::new(
                                EnablerLocality::Local,
                                predicate_builders::All::build_from([
                                    EnablerImpactType::ArbitraryCodeExecution,
                                ])
                            )
                        ),
                        except_if: predicate_builders::Empty::build_from(()),
                        privilege_origin: PrivilegeOrigin::Service,
                        capability_specifier: CapabilitySpecifier::Action,
                        credential_origin: CredentialOrigin::None,
                        credential_type: CredentialType::None,
                    }
                ],
                notes: None,
            },
        );

    map.insert(
        counter.next(),
        ActionDescription {
            name: "Hijack component path or search order, AppCert and AppInit DLL spoofing"
                .to_string(),
            aliases: vec![
                "T1574.006".to_string(),
                "T1574.007".to_string(),
                "T1574.001".to_string(),
                "T1574.011".to_string(),
                "T1546.009".to_string(),
                "T1546.010".to_string(),
            ],
            remote: false,
            uses: vec![
                UsageScheme {
                    given: TokenType::Session | TokenType::Capability,
                    grant: FlagSet::from(TokenType::CapabilityAll),
                    capabilities_required: predicate_builders::All::build_from([
                        CapabilityTemplate {
                            to: CapabilityType::AccessFileSysGeneric,
                            privilege_level: PrivilegeLevel::User,
                            limit: LimitType::Minimum,
                            device: None,
                        },
                        CapabilityTemplate {
                            to: CapabilityType::WriteFiles,
                            privilege_level: PrivilegeLevel::User,
                            limit: LimitType::Minimum,
                            device: None,
                        },
                        CapabilityTemplate {
                            to: CapabilityType::ConfigLocalMachine,
                            privilege_level: PrivilegeLevel::Administrator,
                            limit: LimitType::Minimum,
                            device: None,
                        },
                    ]),
                    capabilities_provided: vec![CapabilityTemplate {
                        to: CapabilityType::Persist,
                        privilege_level: PrivilegeLevel::Service,
                        limit: LimitType::Minimum,
                        device: None,
                    }],
                    enabler_required: None,
                    except_if: predicate_builders::Empty::build_from(()),
                    privilege_origin: PrivilegeOrigin::Fixed(PrivilegeLevel::LocalSystem),
                    capability_specifier: CapabilitySpecifier::Action,
                    credential_origin: CredentialOrigin::None,
                    credential_type: CredentialType::None,
                },
                UsageScheme {
                    given: TokenType::Session | TokenType::Capability,
                    grant: FlagSet::from(TokenType::CapabilityAll),
                    capabilities_required: predicate_builders::All::build_from([
                        CapabilityTemplate {
                            to: CapabilityType::AccessFileSysGeneric,
                            privilege_level: PrivilegeLevel::Service,
                            limit: LimitType::Minimum,
                            device: None,
                        },
                        CapabilityTemplate {
                            to: CapabilityType::WriteFiles,
                            privilege_level: PrivilegeLevel::Service,
                            limit: LimitType::Minimum,
                            device: None,
                        },
                        CapabilityTemplate {
                            to: CapabilityType::ConfigCurrentUser,
                            privilege_level: PrivilegeLevel::Administrator,
                            limit: LimitType::Minimum,
                            device: None,
                        },
                    ]),
                    capabilities_provided: vec![CapabilityTemplate {
                        to: CapabilityType::Persist,
                        privilege_level: PrivilegeLevel::Service,
                        limit: LimitType::Minimum,
                        device: None,
                    }],
                    enabler_required: Some(EnablerTemplate::new(
                        EnablerLocality::Local,
                        predicate_builders::All::build_from([
                            EnablerImpactType::ArbitraryCodeExecution,
                        ]),
                    )),
                    except_if: predicate_builders::Empty::build_from(()),
                    privilege_origin: PrivilegeOrigin::Service,
                    capability_specifier: CapabilitySpecifier::Action,
                    credential_origin: CredentialOrigin::None,
                    credential_type: CredentialType::None,
                },
            ],
            notes: None,
        },
    );
    /* </PrivilegeEscalation> */

    /* <CredentialAccess> */
    // map.insert(
    //     counter.next(),
    //     ActionDescription {
    //         name: "".to_string(),
    //         aliases: vec![],
    //         remote: false,
    //         uses: vec![],
    //         notes: None,
    //     }
    // );
    map
});
