use crate::primitives::shared::enums::{CapabilityType, LimitType, PrivilegeLevel};
use crate::primitives::shared::traits::PrimitiveStrength;
use crate::primitives::shared::types::DeviceHandle;
use std::sync::Arc;

pub trait MatchesTemplate {
    fn matched_by(&self, rhs: &CapabilityTemplate) -> bool;
}

// #[pyclass]
#[derive(Hash, PartialEq, Eq, serde::Serialize, serde::Deserialize, Debug, Clone)]
pub struct CapabilityTemplate {
    pub(crate) to: CapabilityType,
    pub(crate) privilege_level: PrivilegeLevel,
    pub(crate) limit: LimitType,
    pub(crate) device: Option<DeviceHandle>,
}

impl CapabilityTemplate {
    pub fn to(&self) -> &CapabilityType {
        &self.to
    }
    pub fn privilege_level(&self) -> &PrivilegeLevel {
        &self.privilege_level
    }
    pub fn limit(&self) -> &LimitType {
        &self.limit
    }
    pub fn device(&self) -> Option<&DeviceHandle> {
        self.device.as_ref()
    }
}

impl CapabilityTemplate {
    pub fn new(
        to: CapabilityType,
        privilege_level: PrivilegeLevel,
        limit: LimitType,
        device: Option<DeviceHandle>,
    ) -> Self {
        Self {
            to,
            privilege_level,
            limit,
            device,
        }
    }
}

impl MatchesTemplate for CapabilityTemplate {
    fn matched_by(&self, rhs: &Self) -> bool {
        if self.to != rhs.to {
            return false;
        };
        if let Some(lhs_dev) = &self.device {
            if let Some(rhs_dev) = &rhs.device {
                if lhs_dev != rhs_dev {
                    return false;
                };
            };
        };
        match self.limit {
            LimitType::Minimum => self.privilege_level <= rhs.privilege_level,
            LimitType::Exact => self.privilege_level == rhs.privilege_level,
            LimitType::Maximum => return self.privilege_level >= rhs.privilege_level,
        }
    }
}

// #[pymethods]
// impl CapabilityTemplate {
//     #[new]
//     fn init(
//         to: CapabilityType,
//         privilege_level: PrivilegeLevel,
//         limit: LimitType,
//         device: Option<usize>,
//     ) -> Self {
//         Self {
//             to,
//             privilege_level,
//             limit,
//             device,
//         }
//     }
//
//     fn matched_by_template(&self, rhs: &CapabilityTemplate) -> bool {
//         return self.matched_by(rhs);
//     }
// }

// #[pyclass]

#[derive(serde::Serialize, serde::Deserialize, Debug, Clone, PartialEq, Eq, Hash)]
pub struct Capability {
    to: CapabilityType,
    privilege_level: PrivilegeLevel,
    device: DeviceHandle,
}

impl Capability {
    pub fn new(to: CapabilityType, privilege_level: PrivilegeLevel, device: DeviceHandle) -> Self {
        Self {
            to,
            privilege_level,
            device,
        }
    }
    pub fn to(&self) -> &CapabilityType {
        &self.to
    }
    pub fn privilege_level(&self) -> &PrivilegeLevel {
        &self.privilege_level
    }
    pub fn limit(&self) -> LimitType {
        LimitType::Exact
    }
    pub fn device(&self) -> DeviceHandle {
        self.device
    }
}

impl PrimitiveStrength for Capability {
    fn strength(&self) -> u64 {
        self.to.strength() * self.privilege_level.strength()
    }
}

impl MatchesTemplate for Capability {
    fn matched_by(&self, rhs: &CapabilityTemplate) -> bool {
        if self.to != rhs.to {
            return false;
        };

        if let Some(rhs_dev) = rhs.device() {
            if &self.device() != rhs_dev {
                return false;
            };
        };
        match rhs.limit {
            LimitType::Minimum => self.privilege_level >= rhs.privilege_level,
            LimitType::Exact => self.privilege_level == rhs.privilege_level,
            LimitType::Maximum => self.privilege_level <= rhs.privilege_level,
        }
    }
}

#[derive(serde::Serialize, serde::Deserialize, Debug, Clone, PartialEq, Eq, Hash)]
pub struct AttackerCapability {
    base: Arc<Capability>,
    as_user: usize,
}

impl MatchesTemplate for AttackerCapability {
    fn matched_by(&self, rhs: &CapabilityTemplate) -> bool {
        self.base.matched_by(rhs)
    }
}

impl AttackerCapability {
    pub fn new_from_base(base: &Arc<Capability>, as_user: usize) -> Self {
        Self {
            base: Arc::clone(base),
            as_user,
        }
    }
    pub fn is_for_device(&self, target: &DeviceHandle) -> bool {
        &self.base.device == target
    }

    pub fn user(&self) -> &usize {
        &self.as_user
    }
}

impl PrimitiveStrength for AttackerCapability {
    fn strength(&self) -> u64 {
        self.base.strength()
    }
}
