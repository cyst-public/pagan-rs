use crate::error::BackendError;
use crate::primitives::accounts::Account;
use crate::primitives::shared::enums::CredentialType;
use crate::primitives::shared::traits::{ComplexStrength, PrimitiveStrength};
use crate::primitives::shared::types::DeviceHandle;
use crate::primitives::shared::utils::SetOption;
use flagset::FlagSet;
use macros::{compare_optional_weakref_attributes_for_eq, hash_optional_weakref_attribute};
use std::hash::{Hash, Hasher};
use std::ops::BitAnd;
use std::sync::{Arc, Weak};

#[macro_export]
macro_rules! no_wildcards_check {
    ($container: expr, $err: literal) => {
        for cred in &$container {
            match cred.as_ref() {
                Credential::Wildcard(_) => {
                    return Err(BackendError::TopologyError($err.to_string()))
                }
                Credential::Instance(_) => {}
            }
        }
    };
}

macro_rules! choose_best_chain {
    ($ord_collection: expr, $paren_collection: ident, $idx: ident) => {
        loop {
            match $ord_collection.get($idx) {
                None =>  break None,
                Some(cred) => {
                    match cred.as_ref() {
                        Credential::Wildcard(_) => break Some(*cred),
                        Credential::Instance(_) => {
                            let valid_collection = match cred.as_ref() {
                                Credential::Wildcard(_) => {panic!("Chain check can not be used on wildcards!")}
                                Credential::Instance(icred) => {
                                    let mut actual = Arc::clone(cred);
                                    let mut res = true;
                                    while let Some(follower_weak) = icred.next_in_chain() {
                                        match follower_weak.upgrade() {
                                            None => {
                                                res = false;
                                                break
                                            }
                                            Some(follower_strong) => {
                                                if !$paren_collection.contains(&&follower_strong) {
                                                    res = false;
                                                    break
                                                }
                                                actual = match follower_strong.as_ref() {
                                                    Credential::Wildcard(_) => {panic!("Wildcard on cred chain is not allowed!")}
                                                    Credential::Instance(follower) => {
                                                        follower_strong
                                                    }
                                                };
                                            }
                                        }
                                    }
                                    res
                                }
                            };
                            if valid_collection {
                                break Some(*cred)
                            }
                        }
                    }
                }
            }
        };
    }
}

#[derive(Debug, Clone)]
pub struct CredentialInstance {
    r#type: CredentialType,
    device: DeviceHandle,
    services: SetOption<i32>,
    accounts: Vec<Weak<Account>>,
    previous_in_chain: Option<Weak<Credential>>,
    next_in_chain: Option<Weak<Credential>>,
    unseal_complexity: u8,
    online_guess_complexity: u8,
}

impl PartialEq for CredentialInstance {
    fn eq(&self, other: &Self) -> bool {
        if self.r#type().bitand(other.r#type()).bits() == 0 {
            return false;
        }
        if self.device != other.device {
            return false;
        }
        if self.services != other.services {
            return false;
        }
        compare_optional_weakref_attributes_for_eq!(
            self.previous_in_chain,
            other.previous_in_chain
        );
        compare_optional_weakref_attributes_for_eq!(self.next_in_chain, other.next_in_chain);
        if self.unseal_complexity != other.unseal_complexity {
            return false;
        }
        if self.online_guess_complexity != other.online_guess_complexity {
            return false;
        }
        let self_strong_accounts = self
            .accounts
            .iter()
            .filter_map(|weak_ref| weak_ref.upgrade())
            .collect::<Vec<_>>();

        let other_strong_accounts = other
            .accounts
            .iter()
            .filter_map(|weak_ref| weak_ref.upgrade())
            .collect::<Vec<_>>();

        if self_strong_accounts != other_strong_accounts {
            return false;
        }

        true
    }
}

impl Eq for CredentialInstance {}

impl Hash for CredentialInstance {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.r#type.hash(state);
        self.device.hash(state);
        self.online_guess_complexity.hash(state);
        self.unseal_complexity.hash(state);
        self.services.hash(state);
        hash_optional_weakref_attribute!(
            self.next_in_chain,
            Credential,
            state,
            "Credential lost its follower in the chain."
        );
        hash_optional_weakref_attribute!(
            self.previous_in_chain,
            Credential,
            state,
            "Credential lost its follower in the chain."
        );
        self.accounts.iter().for_each(|item| {
            item.upgrade()
                .expect("Credential lost its associated account")
                .hash(state)
        });
    }
}

impl CredentialInstance {
    pub fn new(
        r#type: CredentialType,
        device: DeviceHandle, // TODO: really? should this be a vec as well?
        service: SetOption<i32>,
        accounts: Vec<Weak<Account>>,
        previous_in_chain: Option<Weak<Credential>>,
        next_in_chain: Option<Weak<Credential>>,
        unseal_complexity: u8,
        online_guess_complexity: u8,
    ) -> Self {
        Self {
            r#type,
            device,
            services: service,
            accounts,
            previous_in_chain,
            next_in_chain,
            unseal_complexity,
            online_guess_complexity,
        }
    }
    pub fn r#type(&self) -> CredentialType {
        self.r#type
    }
    pub fn device(&self) -> &DeviceHandle {
        &self.device
    }
    pub fn services(&self) -> &SetOption<i32> {
        &self.services
    }
    pub fn accounts(&self) -> &Vec<Weak<Account>> {
        &self.accounts
    }
    pub fn previous_in_chain(&self) -> &Option<Weak<Credential>> {
        &self.previous_in_chain
    }
    pub fn next_in_chain(&self) -> &Option<Weak<Credential>> {
        &self.next_in_chain
    }
    pub fn unseal_complexity(&self) -> u8 {
        self.unseal_complexity
    }
    pub fn online_guess_complexity(&self) -> u8 {
        self.online_guess_complexity
    }

    pub fn matches_wildcard(&self, wildcard: &CredentialWildcard) -> bool {
        if self.r#type().bitand(wildcard.r#type()).bits() == 0 {
            return false;
        }
        if !wildcard.devices.contains(&self.device) {
            return false;
        }
        self.services.has_intersection(wildcard.services())
            && self
                .accounts
                .iter()
                .filter_map(|item| item.upgrade())
                .any(|item| wildcard.accounts.contains(&item))
    }
}

#[derive(Debug, Clone, PartialEq, Hash, Eq)]
pub struct CredentialWildcard {
    accounts: Vec<Arc<Account>>,
    // We can have an Arc, as an account will never have a Wildcard credential. Thus reference cycles are not a threat.
    devices: Vec<DeviceHandle>,
    services: SetOption<i32>,
    r#type: CredentialType,
}

impl CredentialWildcard {
    pub fn new(
        accounts: Vec<Arc<Account>>,
        devices: Vec<DeviceHandle>,
        services: SetOption<i32>,
        r#type: CredentialType,
    ) -> Self {
        Self {
            accounts,
            devices,
            services,
            r#type,
        }
    }

    pub fn accounts(&self) -> &Vec<Arc<Account>> {
        &self.accounts
    }
    pub fn services(&self) -> &SetOption<i32> {
        &self.services
    }
    pub fn r#type(&self) -> CredentialType {
        self.r#type
    }
    pub fn devices(&self) -> &Vec<DeviceHandle> {
        &self.devices
    }

    pub fn matches_other(&self, rhs: &Self) -> bool {
        if self.r#type().bitand(rhs.r#type()).bits() == 0 {
            return false;
        }
        self.services.has_intersection(rhs.services())
            && self
                .accounts
                .iter()
                .any(|item| rhs.accounts().contains(item))
            && self.devices.iter().any(|dev| rhs.devices().contains(dev))
    }
}

#[derive(Debug, Clone, PartialEq, Hash, Eq)]
pub enum Credential {
    Wildcard(CredentialWildcard),
    Instance(CredentialInstance),
}

impl Credential {
    pub fn r#type(&self) -> CredentialType {
        match self {
            Credential::Wildcard(wc) => wc.r#type(),
            Credential::Instance(ic) => ic.r#type(),
        }
    }

    pub fn services(&self) -> &SetOption<i32> {
        match self {
            Credential::Wildcard(wc) => wc.services(),
            Credential::Instance(ic) => ic.services(),
        }
    }

    pub fn matching(&self, rhs: &Self) -> bool {
        match self {
            Credential::Wildcard(self_wildcard) => match rhs {
                Credential::Wildcard(rhs_wildcard) => self_wildcard.matches_other(rhs_wildcard),
                Credential::Instance(rhs_instance) => rhs_instance.matches_wildcard(self_wildcard),
            },
            Credential::Instance(self_instance) => match rhs {
                Credential::Wildcard(rhs_wildcard) => self_instance.matches_wildcard(rhs_wildcard),
                Credential::Instance(rhs_instance) => self_instance == rhs_instance,
            },
        }
    }

    pub fn is_for_device(&self, device: &DeviceHandle) -> bool {
        match self {
            Credential::Wildcard(wc) => wc.devices.contains(&device),
            Credential::Instance(ic) => ic.device() == device,
        }
    }

    /* Inlined to macro */
    // fn chain_in_collection(cred: &Arc<Credential>, collection: &Vec<Arc<Credential>>) -> bool {
    //     match cred.as_ref() {
    //         Credential::Wildcard(_) => {panic!("Chain check can not be used on wildcards!")}
    //         Credential::Instance(cred) => {
    //             let mut actual = cred;
    //             while let Some(follower_weak) = actual.next_in_chain() {
    //                 match follower_weak.upgrade() {
    //                     None => { return false }
    //                     Some(follower_strong) => {
    //                         if !collection.contains(&follower_strong) {
    //                             return false
    //                         }
    //                         actual = match follower_strong.as_ref() {
    //                             Credential::Wildcard(_) => {panic!("Wildcard on cred chain is not allowed!")}
    //                             Credential::Instance(follower) => {
    //                                 follower
    //                             }
    //                         }
    //                     }
    //                 }
    //             }
    //             true
    //         }
    //     }
    // }

    pub fn select_strongest(
        credentials: &Vec<Arc<Credential>>,
        for_device: DeviceHandle,
    ) -> (Option<&Arc<Credential>>, Option<&Arc<Credential>>) {
        let mut local_ord = credentials
            .iter()
            .filter(|cred| FlagSet::from(cred.r#type()).is_disjoint(CredentialType::AnyPrimary))
            .collect::<Vec<_>>();
        let mut global_ord = local_ord.clone();
        local_ord.sort_by(|cred_a, cred_b| {
            cred_a
                .strength(Some(for_device))
                .cmp(&cred_b.strength(Some(for_device)))
        });
        global_ord.sort_by(|cred_a, cred_b| cred_a.strength(None).cmp(&cred_b.strength(None)));
        let mut idx = 0;
        let local_best = choose_best_chain!(local_ord, credentials, idx);
        idx = 0;
        let global_best = choose_best_chain!(global_ord, credentials, idx);

        (local_best, global_best)
    }
}

impl ComplexStrength for Credential {
    fn strength(&self, target: Option<DeviceHandle>) -> u64 {
        match self {
            Credential::Wildcard(wc) => {
                wc.accounts
                    .iter()
                    .map(|acc| acc.strength(target.clone()))
                    .sum::<u64>()
                    * FlagSet::from(wc.r#type()).strength()
            }
            Credential::Instance(ic) => {
                ic.accounts
                    .iter()
                    .filter_map(|acc_weak| match acc_weak.upgrade() {
                        None => None,
                        Some(acc_strong) => Some(acc_strong.strength(target.clone())),
                    })
                    .sum::<u64>()
                    * FlagSet::from(ic.r#type()).strength()
            }
        }
    }
}
