use crate::primitives::accounts::Account;
use crate::primitives::credentials::Credential;
use crate::primitives::shared::enums::{DeviceRole, OperatingSystem, SegmentType};
use crate::primitives::shared::traits::PrimitiveStrength;
use crate::primitives::shared::utils::Port;
use rand::seq::SliceRandom;
use std::collections::{HashSet, VecDeque};
use std::sync::{Arc, Weak};

pub struct Device {
    name: String,
    vlan: (i32, SegmentType),
    roles: Vec<DeviceRole>,
    os: OperatingSystem,
    prohibited_software: Vec<i32>,
    compulsory_software: Vec<i32>,
    open_ports: Vec<Port>,
    cache_capacity: usize,
    credential_cache: VecDeque<Arc<Credential>>,
    accounts: Vec<Arc<Account>>,
    r#virtual: bool,
    usb_access: bool,
}

impl Device {
    pub fn new(
        name: String,
        vlan: (i32, SegmentType),
        mut roles: Vec<DeviceRole>,
        os: OperatingSystem,
        prohibited_software: Vec<i32>,
        compulsory_software: Vec<i32>,
        open_ports: Vec<Port>,
        cache_capacity: usize,
        accounts: Vec<Arc<Account>>,
        r#virtual: bool,
        usb_access: bool,
    ) -> Self {
        roles.sort_by(|a, b| a.strength().cmp(&b.strength()));
        Self {
            name,
            vlan,
            roles,
            os,
            prohibited_software,
            compulsory_software,
            open_ports,
            cache_capacity,
            credential_cache: VecDeque::with_capacity(cache_capacity),
            accounts,
            r#virtual,
            usb_access,
        }
    }

    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn vlan(&self) -> &(i32, SegmentType) {
        &self.vlan
    }

    pub fn roles(&self) -> &Vec<DeviceRole> {
        &self.roles
    }

    pub fn os(&self) -> &OperatingSystem {
        &self.os
    }

    pub fn prohibited_software(&self) -> &Vec<i32> {
        &self.prohibited_software
    }

    pub fn compulsory_software(&self) -> &Vec<i32> {
        &self.compulsory_software
    }

    pub fn open_ports(&self) -> &Vec<Port> {
        &self.open_ports
    }

    pub fn cache_capacity(&self) -> usize {
        self.cache_capacity
    }

    pub fn credential_cache(&self) -> &VecDeque<Arc<Credential>> {
        &self.credential_cache
    }

    pub fn accounts(&self) -> &Vec<Arc<Account>> {
        &self.accounts
    }

    pub fn r#virtual(&self) -> bool {
        self.r#virtual
    }

    pub fn usb_access(&self) -> bool {
        self.usb_access
    }

    pub fn add_prohibited_software(&mut self, software: i32) {
        self.prohibited_software.push(software)
    }

    pub fn add_compulsory_software(&mut self, software: i32) {
        self.compulsory_software.push(software)
    }

    pub fn add_account(&mut self, account: &Arc<Account>) {
        self.accounts.push(Arc::clone(account))
    }

    pub fn add_system_account(&mut self, account: Account) {
        self.accounts.push(Arc::new(account))
    }

    pub fn get_random_account(&self) -> Weak<Account> {
        let mut rng = rand::thread_rng();
        Arc::downgrade(self.accounts.choose(&mut rng).unwrap())
    }

    pub fn cache_credential(&mut self, credential: &Arc<Credential>) {
        if self.credential_cache.contains(credential) {
            return;
        }
        if self.credential_cache.len() >= self.cache_capacity {
            self.credential_cache.pop_front();
        }
        self.credential_cache.push_back(Arc::clone(credential));
    }

    pub fn get_associated_credentials(&self) -> Vec<Weak<Credential>> {
        let mut credential_set = HashSet::new();
        for account in &self.accounts {
            if account.owner().is_some() {
                match account.owner().as_ref().unwrap().upgrade() {
                    None => continue,
                    Some(person) => {
                        credential_set.extend(std::iter::from_coroutine(person.credentials()))
                    }
                }
            }
        }
        credential_set
            .iter()
            .map(Arc::downgrade)
            .collect::<Vec<_>>()
    }
}

impl PrimitiveStrength for Device {
    fn strength(&self) -> u64 {
        self.roles.iter().map(DeviceRole::strength).sum()
    }
}
