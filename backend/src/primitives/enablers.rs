use crate::entity::sea_orm_active_enums::{EnablerImpactType, EnablerType};
use crate::error::BackendError;
use crate::primitives::predicate::{Predicate, PredicateLeafType};
use crate::primitives::shared::utils::SetOption;
use strum_macros::EnumString;

#[derive(EnumString, Eq, PartialEq, serde::Serialize, serde::Deserialize)]
pub enum EnablerLocality {
    Local,
    Remote,
}

#[derive(serde::Serialize)]
pub struct EnablerTemplate {
    locality: EnablerLocality,
    impact: Predicate<EnablerImpactType>,
}

impl EnablerTemplate {
    pub fn new(locality: EnablerLocality, impact: Predicate<EnablerImpactType>) -> Self {
        Self { locality, impact }
    }
    pub fn locality(&self) -> &EnablerLocality {
        &self.locality
    }
    pub fn impact(&self) -> &Predicate<EnablerImpactType> {
        &self.impact
    }
}

#[derive(serde::Serialize)]
pub struct Enabler {
    kind: EnablerType,
    description: String,
    services: SetOption<i32>,
    actions: SetOption<i32>,
    locality: EnablerLocality,
    impacts: Vec<EnablerImpactType>,
}

impl Enabler {
    fn matches_template(&self, template: &EnablerTemplate) -> Result<bool, BackendError> {
        if self.locality != template.locality {
            return Ok(false);
        }
        template
            .impact()
            .eval(&self.impacts.iter().collect::<Vec<&EnablerImpactType>>())
    }
}
