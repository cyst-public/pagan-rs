use crate::error::BackendError;
use crate::no_wildcards_check;
use crate::primitives::accounts::Account;
use crate::primitives::credentials::Credential;
use std::sync::Arc;

use std::ops::Coroutine;
use std::pin::Pin;

#[derive(PartialEq, Hash)]
pub enum BusinessRole {}

#[derive(PartialEq, Hash)]
pub struct Person {
    accounts: Vec<Arc<Account>>,
    credentials: Vec<Arc<Credential>>,
    role: BusinessRole,
}

impl Person {
    pub fn new(
        accounts: Vec<Arc<Account>>,
        credentials: Vec<Arc<Credential>>,
        role: BusinessRole,
    ) -> Result<Self, BackendError> {
        no_wildcards_check!(
            credentials,
            "Adding wildcard credentials to a person is invalid."
        );
        Ok(Self {
            accounts,
            credentials,
            role,
        })
    }

    pub fn add_account(&mut self, account: Account) {
        self.accounts.push(Arc::new(account))
    }

    pub fn add_credential(&mut self, credential: Arc<Credential>) -> Result<(), BackendError> {
        match credential.as_ref() {
            Credential::Wildcard(_) => Err(BackendError::TopologyError(
                "Adding wildcard credentials to person is invalid.".to_string(),
            )),
            Credential::Instance(_) => {
                self.credentials.push(credential);
                Ok(())
            }
        }
    }

    pub fn credentials(
        &self,
    ) -> Pin<Box<dyn Coroutine<Yield = Arc<Credential>, Return = ()> + '_>> {
        let generator = || {
            for account in &self.accounts {
                for credential in account.credentials() {
                    yield Arc::clone(credential)
                }
            }
            for credential in &self.credentials {
                yield Arc::clone(credential)
            }
        };
        Box::pin(generator)
    }
}
