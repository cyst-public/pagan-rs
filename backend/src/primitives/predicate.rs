use crate::entity::sea_orm_active_enums::EnablerImpactType;
use crate::error::BackendError;
use crate::primitives::adversary::ArtifactSatchel;
use crate::primitives::capabilities::{AttackerCapability, CapabilityTemplate, MatchesTemplate};
use crate::primitives::predicate::Predicate::Not;
use crate::primitives::shared::enums::TokenType;
use flagset::FlagSet;
use std::ops::BitAnd;
use std::sync::Arc;

use erased_serde;
use typetag;

#[typetag::serde(tag = "type")]
pub trait PredicateLeafType {}

#[typetag::serde]
impl PredicateLeafType for TokenType {}
#[typetag::serde]
impl PredicateLeafType for EnablerImpactType {}
#[typetag::serde]
impl PredicateLeafType for CapabilityTemplate {}

#[typetag::serialize(tag = "type")]
pub trait PredicateIterInput {}

#[typetag::serialize(name = "token_ref")]
impl PredicateIterInput for TokenType {}
#[typetag::serialize(name = "impact_ref")]
impl PredicateIterInput for EnablerImpactType {}
#[typetag::serialize(name = "capability_ref")]
impl PredicateIterInput for AttackerCapability {}
#[typetag::serialize(name = "pred_ref")]
impl PredicateIterInput for dyn PredicateLeafType {}

pub trait PredicateNodeFunctionality {
    type Item;

    fn eval_all(&self, elems: &[&Self::Item]) -> Result<bool, BackendError>
    where
        Self::Item: PredicateIterInput + serde::Serialize;
    fn eval_any(&self, elems: &[&Self::Item]) -> Result<bool, BackendError>
    where
        Self::Item: PredicateIterInput + serde::Serialize;
    fn eval_all_satchel(&self, satchel: Arc<ArtifactSatchel>) -> Result<bool, BackendError>;
    fn eval_any_satchel(&self, satchel: Arc<ArtifactSatchel>) -> Result<bool, BackendError>;

    fn atomic_is_present(&self, atomic: &Self::Item) -> bool;
}

pub trait PredicateNode:
    PredicateNodeFunctionality + erased_serde::Serialize + Sync + Send
{
}

#[derive(serde::Serialize)]
pub struct Leaf<T: PredicateLeafType>(Vec<Box<T>>);

#[derive(serde::Serialize)]
pub struct Intermediary<T: PredicateIterInput + serde::Serialize>(Vec<Predicate<T>>);

impl PredicateNodeFunctionality for Leaf<TokenType> {
    type Item = TokenType;

    fn eval_all(&self, elems: &[&Self::Item]) -> Result<bool, BackendError> {
        for item in self.0.iter() {
            if !elems.iter().any(|elem| {
                !FlagSet::from(elem.to_owned().to_owned()).is_disjoint(item.as_ref().to_owned())
            }) {
                return Ok(false);
            }
        }
        Ok(true)
    }

    fn eval_any(&self, elems: &[&Self::Item]) -> Result<bool, BackendError> {
        for item in self.0.iter() {
            if elems.iter().any(|elem| {
                !FlagSet::from(elem.to_owned().to_owned()).is_disjoint(item.as_ref().to_owned())
            }) {
                return Ok(true);
            }
        }
        Ok(false)
    }

    fn eval_all_satchel(&self, satchel: Arc<ArtifactSatchel>) -> Result<bool, BackendError> {
        self.eval_all(
            &satchel
                .tokens()
                .iter()
                .map(|token| token.kind())
                .collect::<Vec<&TokenType>>(),
        )
    }

    fn eval_any_satchel(&self, satchel: Arc<ArtifactSatchel>) -> Result<bool, BackendError> {
        self.eval_any(
            &satchel
                .tokens()
                .iter()
                .map(|token| token.kind())
                .collect::<Vec<&TokenType>>(),
        )
    }

    fn atomic_is_present(&self, atomic: &Self::Item) -> bool {
        self.0.iter().any(|item| item.as_ref() == atomic)
    }
}

impl PredicateNode for Leaf<TokenType> {}

impl PredicateNodeFunctionality for Leaf<EnablerImpactType> {
    type Item = EnablerImpactType;

    fn eval_all(&self, elems: &[&Self::Item]) -> Result<bool, BackendError> {
        for item in self.0.iter() {
            if !elems.iter().any(|elem| *elem == item.as_ref()) {
                return Ok(false);
            }
        }
        Ok(true)
    }

    fn eval_any(&self, elems: &[&Self::Item]) -> Result<bool, BackendError> {
        for item in self.0.iter() {
            if elems.iter().any(|elem| *elem == item.as_ref()) {
                return Ok(true);
            }
        }
        Ok(false)
    }

    fn eval_all_satchel(&self, _satchel: Arc<ArtifactSatchel>) -> Result<bool, BackendError> {
        Err(BackendError::MalformedPredicate)
    }

    fn eval_any_satchel(&self, _satchel: Arc<ArtifactSatchel>) -> Result<bool, BackendError> {
        Err(BackendError::MalformedPredicate)
    }

    fn atomic_is_present(&self, atomic: &Self::Item) -> bool {
        self.0.iter().any(|item| item.as_ref() == atomic)
    }
}

impl PredicateNode for Leaf<EnablerImpactType> {}

impl PredicateNodeFunctionality for Leaf<CapabilityTemplate> {
    type Item = AttackerCapability;

    fn eval_all(&self, elems: &[&Self::Item]) -> Result<bool, BackendError> {
        for item in self.0.iter() {
            if !elems.iter().any(|elem| elem.matched_by(item)) {
                return Ok(false);
            }
        }
        Ok(true)
    }

    fn eval_any(&self, elems: &[&Self::Item]) -> Result<bool, BackendError> {
        for item in self.0.iter() {
            if elems.iter().any(|elem| elem.matched_by(item)) {
                return Ok(true);
            }
        }
        Ok(false)
    }

    fn eval_all_satchel(&self, satchel: Arc<ArtifactSatchel>) -> Result<bool, BackendError> {
        self.eval_all(
            &satchel
                .capabilities()
                .iter()
                .map(Arc::as_ref)
                .collect::<Vec<&AttackerCapability>>(),
        )
    }

    fn eval_any_satchel(&self, satchel: Arc<ArtifactSatchel>) -> Result<bool, BackendError> {
        self.eval_any(
            &satchel
                .capabilities()
                .iter()
                .map(Arc::as_ref)
                .collect::<Vec<&AttackerCapability>>(),
        )
    }

    fn atomic_is_present(&self, atomic: &Self::Item) -> bool {
        self.0.iter().any(|item| atomic.matched_by(item.as_ref()))
    }
}

impl<'a> PredicateNode for Leaf<CapabilityTemplate> {}

impl<'a, T: PredicateIterInput + serde::Serialize> PredicateNodeFunctionality for Intermediary<T> {
    type Item = T;

    fn eval_all(&self, elems: &[&Self::Item]) -> Result<bool, BackendError> {
        for item in self.0.iter() {
            if !item.eval(elems)? {
                return Ok(false);
            }
        }
        Ok(true)
    }

    fn eval_any(&self, elems: &[&Self::Item]) -> Result<bool, BackendError> {
        for item in self.0.iter() {
            if item.eval(elems)? {
                return Ok(true);
            }
        }
        Ok(false)
    }

    fn eval_all_satchel(&self, satchel: Arc<ArtifactSatchel>) -> Result<bool, BackendError> {
        for sub in self.0.iter() {
            if !sub.eval_satchel(Arc::clone(&satchel))? {
                return Ok(false);
            }
        }
        Ok(true)
    }

    fn eval_any_satchel(&self, satchel: Arc<ArtifactSatchel>) -> Result<bool, BackendError> {
        for sub in self.0.iter() {
            if sub.eval_satchel(Arc::clone(&satchel))? {
                return Ok(true);
            }
        }
        Ok(false)
    }

    fn atomic_is_present(&self, atomic: &Self::Item) -> bool {
        self.0
            .iter()
            .any(|sub_predicate| sub_predicate.atomic_is_present(atomic))
    }
}

erased_serde::serialize_trait_object!(<T> PredicateNode<Item = T>);

// #[derive(serde::Serialize, serde::Deserialize)]
// pub enum PredicateNode<T> {
//     Leaf(Leaf<T>),
//     Intermediary(Intermediary<T>),
// }

impl<'a, T: PredicateIterInput + serde::Serialize> PredicateNode for Intermediary<T> {}

#[derive(serde::Serialize)]
pub enum Predicate<T>
where
    T: PredicateIterInput + serde::Serialize,
{
    Empty,
    Not(Box<Predicate<T>>),
    All(Box<dyn PredicateNode<Item = T>>),
    Any(Box<dyn PredicateNode<Item = T>>),
}

impl<T: PredicateIterInput + serde::Serialize> Predicate<T> {
    pub fn eval(&self, elems: &[&T]) -> Result<bool, BackendError> {
        match self {
            Predicate::Empty => Ok(true),
            Not(pred) => Ok(!pred.eval(elems)?),
            Predicate::All(inner) => inner.eval_all(elems),
            Predicate::Any(inner) => inner.eval_any(elems),
        }
    }

    pub fn eval_satchel(&self, satchel: Arc<ArtifactSatchel>) -> Result<bool, BackendError> {
        match self {
            Predicate::Empty => Ok(true),
            Not(pred) => Ok(!pred.eval_satchel(satchel)?),
            Predicate::All(inner) => inner.eval_all_satchel(satchel),
            Predicate::Any(inner) => inner.eval_any_satchel(satchel),
        }
    }

    pub fn atomic_is_present(&self, atomic: &T) -> bool {
        match self {
            Predicate::Empty => false,
            Not(pred) => pred.atomic_is_present(atomic),
            Predicate::All(inner) => inner.atomic_is_present(atomic),
            Predicate::Any(inner) => inner.atomic_is_present(atomic),
        }
    }
}

pub mod predicate_builders {
    use crate::entity::sea_orm_active_enums::EnablerImpactType;
    use crate::primitives::capabilities::{AttackerCapability, CapabilityTemplate};
    use crate::primitives::predicate::Predicate;
    use crate::primitives::predicate::{Intermediary, Leaf, PredicateIterInput};
    use crate::primitives::shared::enums::TokenType;
    use strum::IntoEnumIterator;

    pub trait Builder<I, O: PredicateIterInput + 'static + serde::Serialize> {
        fn build_from(value: I) -> Predicate<O>;
    }

    pub struct Not {}
    pub struct Empty {}
    pub struct All {}
    pub struct Any {}
    pub struct NoneOf {}
    pub struct NotJust {}

    impl<const N: usize> Builder<[TokenType; N], TokenType> for All {
        fn build_from(value: [TokenType; N]) -> Predicate<TokenType> {
            let mut contents = Vec::with_capacity(value.len());
            value
                .into_iter()
                .for_each(|elem| contents.push(Box::new(elem)));
            Predicate::All(Box::new(Leaf(contents)))
        }
    }

    impl<const N: usize> Builder<[CapabilityTemplate; N], AttackerCapability> for All {
        fn build_from(value: [CapabilityTemplate; N]) -> Predicate<AttackerCapability> {
            let mut contents = Vec::with_capacity(value.len());
            value
                .into_iter()
                .for_each(|elem| contents.push(Box::new(elem)));
            Predicate::All(Box::new(Leaf(contents)))
        }
    }

    impl<const N: usize> Builder<[EnablerImpactType; N], EnablerImpactType> for All {
        fn build_from(value: [EnablerImpactType; N]) -> Predicate<EnablerImpactType> {
            let mut contents = Vec::with_capacity(value.len());
            value
                .into_iter()
                .for_each(|elem| contents.push(Box::new(elem)));
            Predicate::All(Box::new(Leaf(contents)))
        }
    }

    impl<const N: usize, O: PredicateIterInput + 'static + serde::Serialize>
        Builder<[Predicate<O>; N], O> for All
    {
        fn build_from(value: [Predicate<O>; N]) -> Predicate<O> {
            Predicate::All(Box::new(Intermediary(Vec::from(value))))
        }
    }

    // impl Builder<&[TokenType], TokenType> for All {
    //     fn build_from(value: &[TokenType]) -> Predicate<TokenType> {
    //         let mut contents = Vec::with_capacity(value.len());
    //         value
    //             .into_iter()
    //             .for_each(|elem| contents.push(Box::new(elem.clone())));
    //         Predicate::All(Box::new(Leaf(contents)))
    //     }
    // }
    //
    // impl Builder<&[CapabilityTemplate], AttackerCapability> for All {
    //     fn build_from(value: &[CapabilityTemplate]) -> Predicate<AttackerCapability> {
    //         let mut contents = Vec::with_capacity(value.len());
    //         value
    //             .into_iter()
    //             .for_each(|elem| contents.push(Box::new(elem.clone())));
    //         Predicate::All(Box::new(Leaf(contents)))
    //     }
    // }
    //
    // impl Builder<&[EnablerImpactType], EnablerImpactType> for All {
    //     fn build_from(value: &[EnablerImpactType]) -> Predicate<EnablerImpactType> {
    //         let mut contents = Vec::with_capacity(value.len());
    //         value
    //             .into_iter()
    //             .for_each(|elem| contents.push(Box::new(elem.clone())));
    //         Predicate::All(Box::new(Leaf(contents)))
    //     }
    // }
    //
    // impl<O: PredicateIterInput + 'static + serde::Serialize> Builder<&[Predicate<O>], O> for All {
    //     fn build_from(value: &[Predicate<O>]) -> Predicate<O> {
    //         Predicate::All(Box::new(Intermediary(Vec::from(value))))
    //     }
    // }

    impl<const N: usize> Builder<[TokenType; N], TokenType> for Any {
        fn build_from(value: [TokenType; N]) -> Predicate<TokenType> {
            let mut contents = Vec::with_capacity(value.len());
            value
                .into_iter()
                .for_each(|elem| contents.push(Box::new(elem)));
            Predicate::Any(Box::new(Leaf(contents)))
        }
    }

    impl<const N: usize> Builder<[CapabilityTemplate; N], AttackerCapability> for Any {
        fn build_from(value: [CapabilityTemplate; N]) -> Predicate<AttackerCapability> {
            let mut contents = Vec::with_capacity(value.len());
            value
                .into_iter()
                .for_each(|elem| contents.push(Box::new(elem)));
            Predicate::Any(Box::new(Leaf(contents)))
        }
    }

    impl<const N: usize> Builder<[EnablerImpactType; N], EnablerImpactType> for Any {
        fn build_from(value: [EnablerImpactType; N]) -> Predicate<EnablerImpactType> {
            let mut contents = Vec::with_capacity(value.len());
            value
                .into_iter()
                .for_each(|elem| contents.push(Box::new(elem)));
            Predicate::Any(Box::new(Leaf(contents)))
        }
    }

    impl<const N: usize, O: PredicateIterInput + 'static + serde::Serialize>
        Builder<[Predicate<O>; N], O> for Any
    {
        fn build_from(value: [Predicate<O>; N]) -> Predicate<O> {
            Predicate::Any(Box::new(Intermediary(Vec::from(value))))
        }
    }

    impl<O: PredicateIterInput + 'static + serde::Serialize> Builder<Predicate<O>, O> for Not {
        fn build_from(value: Predicate<O>) -> Predicate<O> {
            Predicate::Not(Box::new(value))
        }
    }

    impl Builder<TokenType, TokenType> for Not {
        fn build_from(value: TokenType) -> Predicate<TokenType> {
            super::predicate_builders::Not::build_from(super::predicate_builders::Any::build_from(
                [value],
            ))
        }
    }

    impl Builder<EnablerImpactType, EnablerImpactType> for Not {
        fn build_from(value: EnablerImpactType) -> Predicate<EnablerImpactType> {
            super::predicate_builders::Not::build_from(super::predicate_builders::Any::build_from(
                [value],
            ))
        }
    }

    impl Builder<CapabilityTemplate, AttackerCapability> for Not {
        fn build_from(value: CapabilityTemplate) -> Predicate<AttackerCapability> {
            super::predicate_builders::Not::build_from(super::predicate_builders::Any::build_from(
                [value],
            ))
        }
    }

    impl<const N: usize> Builder<[TokenType; N], TokenType> for NoneOf {
        fn build_from(value: [TokenType; N]) -> Predicate<TokenType> {
            let mut contents = Vec::with_capacity(value.len());
            value
                .into_iter()
                .for_each(|elem| contents.push(Box::new(elem)));
            Not::build_from(Predicate::Any(Box::new(Leaf(contents))))
        }
    }

    impl<const N: usize> Builder<[CapabilityTemplate; N], AttackerCapability> for NoneOf {
        fn build_from(value: [CapabilityTemplate; N]) -> Predicate<AttackerCapability> {
            let mut contents = Vec::with_capacity(value.len());
            value
                .into_iter()
                .for_each(|elem| contents.push(Box::new(elem)));
            Not::build_from(Predicate::Any(Box::new(Leaf(contents))))
        }
    }

    impl<const N: usize> Builder<[EnablerImpactType; N], EnablerImpactType> for NoneOf {
        fn build_from(value: [EnablerImpactType; N]) -> Predicate<EnablerImpactType> {
            let mut contents = Vec::with_capacity(value.len());
            value
                .into_iter()
                .for_each(|elem| contents.push(Box::new(elem)));
            Not::build_from(Predicate::Any(Box::new(Leaf(contents))))
        }
    }

    impl<const N: usize, O: PredicateIterInput + 'static + serde::Serialize>
        Builder<[Predicate<O>; N], O> for NoneOf
    {
        fn build_from(value: [Predicate<O>; N]) -> Predicate<O> {
            Not::build_from(Predicate::Any(Box::new(Intermediary(Vec::from(value)))))
        }
    }

    impl<const N: usize> Builder<[TokenType; N], TokenType> for NotJust {
        fn build_from(value: [TokenType; N]) -> Predicate<TokenType> {
            let remainders = TokenType::iter()
                .filter(|variant| !value.contains(variant))
                .map(Box::new)
                .collect::<Vec<Box<TokenType>>>();

            super::predicate_builders::All::build_from([
                super::predicate_builders::All::build_from(value),
                Predicate::Any(Box::new(Leaf(remainders))),
            ])
        }
    }

    impl<const N: usize> Builder<[EnablerImpactType; N], EnablerImpactType> for NotJust {
        fn build_from(value: [EnablerImpactType; N]) -> Predicate<EnablerImpactType> {
            let remainders = EnablerImpactType::iter()
                .filter(|variant| !value.contains(variant))
                .map(Box::new)
                .collect::<Vec<Box<EnablerImpactType>>>();

            super::predicate_builders::All::build_from([
                super::predicate_builders::All::build_from(value),
                Predicate::Any(Box::new(Leaf(remainders))),
            ])
        }
    }

    impl<T: PredicateIterInput + 'static + serde::Serialize> Builder<(), T> for Empty {
        fn build_from(_value: ()) -> Predicate<T> {
            Predicate::Empty
        }
    }
}

#[cfg(test)]
mod test {
    use crate::entity::sea_orm_active_enums::EnablerImpactType;
    use crate::primitives::capabilities::CapabilityTemplate;
    use crate::primitives::predicate::predicate_builders::Builder;
    use crate::primitives::shared::enums::{CapabilityType, LimitType, PrivilegeLevel, TokenType};

    #[test]
    fn eval_all_tokens() {
        let pred = super::predicate_builders::All::build_from([
            TokenType::Capability,
            TokenType::Session,
            TokenType::TaintedSharedContent,
        ]);
        assert!(pred
            .eval(&vec![
                &TokenType::Capability,
                &TokenType::Session,
                &TokenType::TaintedSharedContent,
                &TokenType::AccessDmzSegment,
            ],)
            .unwrap());

        assert!(!pred
            .eval(&vec![
                &TokenType::CapabilityAll,
                &TokenType::TaintedSharedContent,
                &TokenType::AccessDmzSegment
            ])
            .unwrap());
        assert!(pred
            .eval(&vec![
                &TokenType::CapabilityAll,
                &TokenType::Session,
                &TokenType::TaintedSharedContent,
                &TokenType::AccessDmzSegment,
            ],)
            .unwrap());
    }

    #[test]
    fn eval_capabilities() {
        let pred = super::predicate_builders::All::build_from([
            CapabilityTemplate {
                to: CapabilityType::AccessFileSysGeneric,
                privilege_level: PrivilegeLevel::Administrator,
                limit: LimitType::Minimum,
                device: Some(1),
            },
            CapabilityTemplate {
                to: CapabilityType::WriteFiles,
                privilege_level: PrivilegeLevel::Administrator,
                limit: LimitType::Minimum,
                device: None,
            },
        ]);

        //     assert!(pred
        //         .eval(&vec![
        //             &AttackerCapability::new(
        //                 CapabilityType::AccessFileSysGeneric,
        //                 PrivilegeLevel::Administrator,
        //                 LimitType::Minimum,
        //                 Some(1),
        //                 None
        //             ),
        //             &AttackerCapability::new(
        //                 CapabilityType::WriteFiles,
        //                 PrivilegeLevel::Administrator,
        //                 LimitType::Minimum,
        //                 None,
        //                 None
        //             ),
        //             &AttackerCapability::new(
        //                 CapabilityType::ReadFiles,
        //                 PrivilegeLevel::Administrator,
        //                 LimitType::Minimum,
        //                 None,
        //                 None
        //             )
        //         ])
        //         .unwrap());
        //     assert!(!pred
        //         .eval(&vec![
        //             &AttackerCapability::new(
        //                 CapabilityType::AccessFileSysGeneric,
        //                 PrivilegeLevel::Administrator,
        //                 LimitType::Minimum,
        //                 Some(1),
        //                 None
        //             ),
        //             &AttackerCapability::new(
        //                 CapabilityType::ReadFiles,
        //                 PrivilegeLevel::Administrator,
        //                 LimitType::Minimum,
        //                 None,
        //                 None
        //             )
        //         ])
        //         .unwrap())
    }

    #[test]
    fn eval_all_preds_uniform() {
        let pred_a = super::predicate_builders::All::build_from([
            TokenType::Session,
            TokenType::Containerized,
        ]);
        let pred_b =
            super::predicate_builders::All::build_from([TokenType::Session, TokenType::Credential]);

        let pred = super::predicate_builders::All::build_from([pred_a, pred_b]);

        assert!(pred
            .eval(&vec![
                &TokenType::Credential,
                &TokenType::Session,
                &TokenType::Containerized
            ])
            .unwrap());
        assert!(!pred
            .eval(&vec![
                &TokenType::AccessDmzSegment,
                &TokenType::Session,
                &TokenType::Containerized
            ])
            .unwrap())
    }

    #[test]
    fn eval_all_preds_non_uniform() {
        let pred_a = super::predicate_builders::All::build_from([
            TokenType::Session,
            TokenType::Containerized,
        ]);
        let pred_b = super::predicate_builders::All::build_from([
            EnablerImpactType::Allow,
            EnablerImpactType::ArbitraryCodeExecution,
        ]);

        let pred = super::predicate_builders::All::build_from([pred_a, pred_b]);

        assert!(pred
            .eval(&vec![
                &TokenType::Credential,
                &TokenType::Session,
                &TokenType::Containerized
            ])
            .unwrap());
        assert!(!pred
            .eval(&vec![
                &TokenType::AccessDmzSegment,
                &TokenType::Session,
                &TokenType::Containerized
            ])
            .unwrap())
    }
}
