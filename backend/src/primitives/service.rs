use crate::entity::sea_orm_active_enums::{DbPrivilegeLevel, Impact, Tag};
use crate::error::BackendError;
use crate::primitives::shared::enums::{CapabilityType, PrivilegeLevel, ProcessIntegrityLevel};
use crate::primitives::shared::traits::PrimitiveStrength;
use crate::primitives::shared::utils::Port;
use pyo3::ffi::PyExc_RuntimeError;
use pyo3::types::PyString;
use pyo3::{pyclass, pymethods, PyErr, PyResult};
use sea_orm::FromQueryResult;
use std::cmp::min;

// #[pyclass]
pub struct ProcessInfo {
    executable: Service,
    integrity: ProcessIntegrityLevel,
    ports_held: Vec<Port>,
}

#[pyclass]
#[derive(Debug, FromQueryResult, serde::Deserialize, serde::Serialize)]
pub struct Service {
    pub id: i32,
    pub name: String,
    pub version: String,
    pub vendor: String,
    pub auto_elevation: Option<bool>,
    pub executable_access: Option<DbPrivilegeLevel>,
    pub target_software: Option<String>,
    pub ports: Option<Vec<i32>>,
    pub tags: Vec<Tag>,
    pub impacts: Vec<Impact>,
}

#[pymethods]
impl Service {
    fn __repr__(&self) -> String {
        format!("{:?}", self)
    }

    fn __str__(&self) -> String {
        format!("{:?}", self)
    }

    pub fn id(&self) -> i32 {
        self.id
    }
    pub fn name(&self) -> &str {
        self.name.as_str()
    }

    pub fn version(&self) -> &str {
        self.version.as_str()
    }

    pub fn vendor(&self) -> &str {
        self.vendor.as_str()
    }

    pub fn auto_elevation(&self) -> Option<bool> {
        self.auto_elevation
    }

    pub fn executable_access(&self) -> Option<DbPrivilegeLevel> {
        self.executable_access.clone()
    }

    pub fn target_software(&self) -> Option<&str> {
        match &self.target_software {
            None => None,
            Some(content) => Some(content.as_str()),
        }
    }

    pub fn ports(&self) -> Option<Vec<i32>> {
        self.ports.clone()
    }

    pub fn tags(&self) -> Vec<Tag> {
        self.tags.clone()
    }

    pub fn impacts(&self) -> Vec<Impact> {
        self.impacts.clone()
    }

    pub fn impacts_as_capabilities(&self) -> Vec<CapabilityType> {
        let mut res = Vec::new();
        for impact in &self.impacts {
            match impact {
                Impact::ExecuteCode => res.extend_from_slice(&[
                    CapabilityType::AccessFileSysGeneric,
                    CapabilityType::AccessFileSysElevated,
                    CapabilityType::AccessFileSysShared,
                    CapabilityType::ReadFiles,
                    CapabilityType::WriteFiles,
                    CapabilityType::AccessNetworkAsDomain,
                    CapabilityType::AddUser,
                    CapabilityType::EditUser,
                    CapabilityType::DeleteUser,
                    CapabilityType::PersistLogOut,
                    CapabilityType::CodeExecution,
                    CapabilityType::Shutdown,
                    CapabilityType::DomainLogin,
                    CapabilityType::Impersonate,
                    CapabilityType::ControlDomain,
                    CapabilityType::ConfigLocalMachine,
                    CapabilityType::ConfigCurrentUser,
                    CapabilityType::SeDebug,
                    CapabilityType::SeLoadDriver,
                    CapabilityType::SeCreateSecurityToken,
                    CapabilityType::SeImpersonateToken,
                    CapabilityType::UacElevate,
                ]),
                Impact::ReadFile => res.extend_from_slice(&[
                    CapabilityType::AccessFileSysGeneric,
                    CapabilityType::AccessFileSysElevated,
                    CapabilityType::AccessFileSysShared,
                    CapabilityType::ReadFiles,
                ]),
                Impact::UseNetwork => {}
                Impact::WriteFile => res.extend_from_slice(&[
                    CapabilityType::AccessFileSysGeneric,
                    CapabilityType::AccessFileSysElevated,
                    CapabilityType::AccessFileSysShared,
                    CapabilityType::WriteFiles,
                ]),
            }
        }
        return res;
    }

    pub fn get_effective_privilege(&self) -> u64 {
        match &self.auto_elevation {
            None => PrivilegeLevel::from(self.executable_access.clone()).strength(),
            Some(flag) => match flag {
                true => match &self.executable_access {
                    None => PrivilegeLevel::None.strength(),
                    Some(access) => match access {
                        DbPrivilegeLevel::Administrator => PrivilegeLevel::LocalSystem.strength(),
                        DbPrivilegeLevel::User => PrivilegeLevel::Administrator.strength(),
                        _ => PrivilegeLevel::from(self.executable_access.clone()).strength(),
                    },
                },
                false => PrivilegeLevel::from(self.executable_access.clone()).strength(),
            },
        }
    }

    pub fn get_running_integrity(&self, spawner_privilege: &PrivilegeLevel) -> u64 {
        if let Some(true) = self.auto_elevation {
            self.get_effective_privilege()
        } else {
            min(spawner_privilege.strength(), self.get_effective_privilege())
        }
    }

    #[staticmethod]
    pub fn from_json(json: String) -> PyResult<Self> {
        match serde_json::from_str::<Self>(json.as_str()) {
            Ok(specimen) => Ok(specimen),
            Err(e) => Err(PyErr::from(BackendError::SerdeJsonError(e))),
        }
    }
}
