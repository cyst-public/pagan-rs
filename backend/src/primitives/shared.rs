pub mod traits {
    use crate::primitives::shared::types::DeviceHandle;

    pub trait PrimitiveStrength {
        fn strength(&self) -> u64;
    }

    pub trait ComplexStrength {
        fn strength(&self, target: Option<DeviceHandle>) -> u64;
    }
}

pub mod enums {
    use crate::entity::sea_orm_active_enums::DbPrivilegeLevel;
    use crate::primitives::shared::traits::PrimitiveStrength;
    use flagset::{flags, FlagSet};
    use pyo3::{pyclass, pymethods};
    use rand::seq::SliceRandom;
    use std::ops::BitAnd;
    use strum_macros::{EnumIter, EnumString};

    flags! {
        #[pyclass]
        #[repr(u64)]
        #[derive(Hash, EnumString, EnumIter, serde::Serialize, serde::Deserialize)]
        pub enum TokenType : u64 {
            None = 1 << 0,

            Credential = 1 << 1,
            CredentialMfa = 1 << 2,

            Capability = 1 << 3,
            CapabilityAll = 1 << 3 | 1 << 4,

            Data = 1 << 5,
            ImpactDenialOfService = 1 << 6,

            AccessViaNetwork = 1 << 7,
            AccessViaSharedFilesys = 1 << 8,
            AccessViaRemovableMedia = 1 << 9,

            Session = 1 << 10,

            PreAttackBackdoor = 1 << 11,
            ExternalPhishingMail = 1 << 12,

            TaintedSharedContent = 1 << 13,
            InfectedRemovableMedia = 1 << 14,
            InternalPhishingMail = 1 <<15,
            MaliciousDomainConfigurationObject = 1 << 16,

            PhysicalAccess = 1 << 17,

            AccessInternalNetwork = 1 << 18,
            AccessDmzSegment = 1 << 19,
            Containerized =  1 << 20,
            NoUsbAccess = 1 << 21,
            Ongoing = 1 << 22,
            NoMfaAllowed = 1 << 23
        }

        #[repr(u64)]
        #[derive(EnumString, serde::Serialize, serde::Deserialize, Hash)]
        pub enum CredentialType: u64 {
            None = 1 << 0,
            Os = 1 << 1,
            AppPrimary = 1 << 2,
            MfaPassable = 1 << 3,
            MfaSecure = 1 << 4,
            Mfa = (CredentialType::MfaPassable | CredentialType::MfaSecure).bits(),
            AnyPrimary = (CredentialType::Os | CredentialType::AppPrimary).bits(),
            AppAnyPassable = (CredentialType::AppPrimary | CredentialType::MfaPassable).bits(),
            AppAny = (CredentialType::AppPrimary | CredentialType::Mfa).bits(),
            Unspec = (CredentialType::AnyPrimary | CredentialType::Mfa).bits()
        }
    }

    impl PrimitiveStrength for FlagSet<CredentialType> {
        fn strength(&self) -> u64 {
            let mut res = 0u64;
            if !self.is_disjoint(CredentialType::Os) {
                res += 6;
            }
            if !self.is_disjoint(CredentialType::AppPrimary) {
                res += 3;
            }
            if !self.is_disjoint(CredentialType::MfaPassable) {
                res += 1;
            }
            if !self.is_disjoint(CredentialType::MfaSecure) {
                res += 0;
            }

            res
        }
    }

    #[pyclass]
    #[derive(
        EnumString,
        Clone,
        PartialEq,
        Hash,
        Eq,
        EnumIter,
        serde::Serialize,
        serde::Deserialize,
        Debug,
    )]
    pub enum CapabilityType {
        AccessFileSysGeneric,
        AccessFileSysElevated,
        AccessFileSysShared,
        ReadFiles,
        WriteFiles,
        AccessNetworkAsDomain,
        AddUser,
        EditUser,
        DeleteUser,
        PersistLogOut,
        CodeExecution,
        Shutdown,
        DomainLogin,
        Persist,
        Impersonate,
        ControlDomain,
        ConfigLocalMachine,
        ConfigCurrentUser,
        SeDebug,
        SeLoadDriver,
        SeCreateSecurityToken,
        SeImpersonateToken,
        UacElevate,
    }

    impl PrimitiveStrength for CapabilityType {
        fn strength(&self) -> u64 {
            match self {
                CapabilityType::AccessFileSysGeneric => 15,
                CapabilityType::AccessFileSysElevated => 20,
                CapabilityType::AccessFileSysShared => 18,
                CapabilityType::ReadFiles => 30,
                CapabilityType::WriteFiles => 35,
                CapabilityType::AccessNetworkAsDomain => 10,
                CapabilityType::AddUser => 25,
                CapabilityType::EditUser => 25,
                CapabilityType::DeleteUser => 25,
                CapabilityType::PersistLogOut => 10,
                CapabilityType::CodeExecution => 45,
                CapabilityType::Shutdown => 10,
                CapabilityType::DomainLogin => 40,
                CapabilityType::Persist => 5,
                CapabilityType::Impersonate => 5,
                CapabilityType::ControlDomain => 80,
                CapabilityType::ConfigLocalMachine => 40,
                CapabilityType::ConfigCurrentUser => 25,
                CapabilityType::SeDebug => 35,
                CapabilityType::SeLoadDriver => 40,
                CapabilityType::SeCreateSecurityToken => 40,
                CapabilityType::SeImpersonateToken => 40,
                CapabilityType::UacElevate => 20,
            }
        }
    }

    #[pyclass]
    #[derive(
        EnumString, Clone, PartialEq, Hash, Eq, serde::Serialize, serde::Deserialize, Debug,
    )]
    pub enum LimitType {
        Minimum,
        Exact,
        Maximum,
    }

    #[pyclass]
    #[derive(
        Ord,
        PartialOrd,
        PartialEq,
        Eq,
        EnumString,
        Clone,
        Hash,
        serde::Serialize,
        serde::Deserialize,
        Debug,
    )]
    pub enum PrivilegeLevel {
        None,
        Service,
        User,
        Administrator,
        LocalSystem,
    }

    impl PrimitiveStrength for PrivilegeLevel {
        fn strength(&self) -> u64 {
            self.to_owned() as u64
        }
    }

    impl Default for PrivilegeLevel {
        fn default() -> Self {
            Self::None
        }
    }

    #[pyclass]
    #[derive(
        Ord,
        PartialOrd,
        PartialEq,
        Eq,
        EnumString,
        Clone,
        Hash,
        serde::Serialize,
        serde::Deserialize,
    )]
    pub enum ProcessIntegrityLevel {
        Low,
        Medium,
        High,
        System,
        Protected,
    }

    impl PrimitiveStrength for ProcessIntegrityLevel {
        fn strength(&self) -> u64 {
            self.clone() as u64
        }
    }

    impl From<PrivilegeLevel> for ProcessIntegrityLevel {
        fn from(value: PrivilegeLevel) -> Self {
            match value {
                PrivilegeLevel::None => {
                    panic!("Cannot convert PrivilegeLevel::None to ProcessIntegrityLevel")
                }
                PrivilegeLevel::Service => ProcessIntegrityLevel::Low,
                PrivilegeLevel::User => ProcessIntegrityLevel::Medium,
                PrivilegeLevel::Administrator => ProcessIntegrityLevel::High,
                PrivilegeLevel::LocalSystem => ProcessIntegrityLevel::System,
            }
        }
    }

    impl From<Option<crate::entity::sea_orm_active_enums::DbPrivilegeLevel>> for PrivilegeLevel {
        fn from(value: Option<crate::entity::sea_orm_active_enums::DbPrivilegeLevel>) -> Self {
            if let Some(val) = value {
                match val {
                    DbPrivilegeLevel::Administrator => Self::Administrator,
                    DbPrivilegeLevel::LocalSystem => Self::LocalSystem,
                    DbPrivilegeLevel::None => Self::None,
                    DbPrivilegeLevel::Service => Self::Service,
                    DbPrivilegeLevel::User => Self::User,
                }
            } else {
                PrivilegeLevel::None
            }
        }
    }

    // #[pyclass]
    #[derive(PartialEq, EnumString, Clone, serde::Serialize, serde::Deserialize)]
    pub enum PrivilegeOrigin {
        None,
        Service,
        Credential,
        RandomUser,
        NewUser,
        InheritUser,
        ElevatedUser,
        Fixed(PrivilegeLevel),
    }

    #[pyclass]
    #[derive(PartialEq, EnumString, Clone, serde::Serialize, serde::Deserialize)]
    pub enum CapabilitySpecifier {
        None,
        Service,
        Enabler,
        Action,
    }

    #[derive(PartialEq, EnumString, Clone, serde::Serialize, serde::Deserialize)]
    pub enum CredentialOrigin {
        None,
        OsAllLocal,
        OsCached,
        OsActiveAccount,
        ServiceSelf,
        ServiceSelfAll,
        ServiceOther,
        Mimic,
        MimicUser,
        MimicAll,
        AnyActualUser,
        AnyPresentUsers,
        NewUser,
    }

    #[pyclass]
    #[derive(PartialEq, Clone, serde::Serialize, serde::Deserialize)]
    pub enum AccessType {
        Network,
        Media,
        SharedFileSystem,
    }

    #[pyclass]
    #[derive(PartialEq, Clone, serde::Serialize, serde::Deserialize, EnumString)]
    pub enum OperatingSystem {
        WindowsServer,
        WindowsDesktop,
        NixServer,
        NixDesktop,
        SpecialNetwork,
        SpecialEmbedded,
        Mock,
    }

    #[pymethods]
    impl OperatingSystem {
        #[staticmethod]
        pub fn random_server() -> Self {
            let mut rng = rand::thread_rng();
            [Self::WindowsServer, Self::NixServer]
                .choose(&mut rng)
                .unwrap()
                .to_owned()
        }

        #[staticmethod]
        pub fn random_desktop() -> Self {
            let mut rng = rand::thread_rng();
            [Self::WindowsDesktop, Self::NixDesktop]
                .choose(&mut rng)
                .unwrap()
                .to_owned()
        }
    }

    #[derive(PartialEq, Clone, serde::Serialize, serde::Deserialize, EnumString)]
    pub enum DeviceRole {
        Workstation,
        MailServer,
        WebServer,
        DomainController,
        NetworkShare,
        BackupServer,
        Database,
        OperationalTechnology,
        NetworkAppliance,
        RemoteAccessGateway,
    }

    impl PrimitiveStrength for DeviceRole {
        fn strength(&self) -> u64 {
            match self {
                DeviceRole::Workstation => 15,
                DeviceRole::MailServer => 25,
                DeviceRole::WebServer => 25,
                DeviceRole::DomainController => 100,
                DeviceRole::NetworkShare => 55,
                DeviceRole::BackupServer => 85,
                DeviceRole::Database => 45,
                DeviceRole::OperationalTechnology => 35,
                DeviceRole::NetworkAppliance => 20,
                DeviceRole::RemoteAccessGateway => 30,
            }
        }
    }

    #[pyclass]
    #[derive(PartialEq, Clone, serde::Serialize, serde::Deserialize)]
    pub enum SegmentType {
        Internal,
        Dmz,
        Attacker,
        Partner,
        Internet,
    }

    impl Into<TokenType> for SegmentType {
        fn into(self) -> TokenType {
            match &self {
                SegmentType::Internal => TokenType::AccessInternalNetwork,
                SegmentType::Dmz => TokenType::AccessDmzSegment,
                _ => TokenType::None,
            }
        }
    }
}

pub mod artifacts {
    use crate::primitives::shared::enums::TokenType;
    use crate::primitives::shared::types::DeviceHandle;
    use pyo3::pyclass;

    #[pyclass]
    #[derive(Debug, Clone, PartialEq, Hash, Eq)]
    pub struct Token {
        kind: TokenType,
        locality: Option<DeviceHandle>,
    }

    impl Token {
        pub fn kind(&self) -> &TokenType {
            &self.kind
        }
        pub fn locality(&self) -> &Option<DeviceHandle> {
            &self.locality
        }

        pub fn is_for_device(&self, device: &DeviceHandle) -> bool {
            match self.locality {
                None => true,
                Some(loc) => &loc == device,
            }
        }
    }
}

pub mod utils {
    use crate::error::BackendError;
    use log::warn;
    use pyo3::{pyclass, pymethods, IntoPy, PyObject};
    use std::collections::BTreeSet;
    use std::hash::Hash;
    use std::ops::Range;

    #[derive(Default)]
    pub struct Counter {
        ct: i32,
    }

    impl Counter {
        pub fn next(&mut self) -> i32 {
            self.ct += 1;
            self.ct
        }
    }

    #[derive(serde::Serialize, serde::Deserialize, Debug, Clone, PartialEq, Hash, Eq)]
    pub enum SetOption<T: Hash + Eq + Ord> {
        None,
        All,
        Some(BTreeSet<T>),
    }

    impl<T: Hash + Eq + Ord + Clone> SetOption<T> {
        pub fn union(&self, rhs: &Self) -> Self {
            match self {
                SetOption::None => rhs.clone(),
                SetOption::All => self.clone(),
                SetOption::Some(s) => match rhs {
                    SetOption::None => self.clone(),
                    SetOption::All => rhs.clone(),
                    SetOption::Some(o) => SetOption::Some(BTreeSet::from_iter(s.union(o).cloned())),
                },
            }
        }

        pub fn intersection(&self, rhs: &Self) -> Self {
            match self {
                SetOption::None => self.clone(),
                SetOption::All => rhs.clone(),
                SetOption::Some(s) => match rhs {
                    SetOption::None => rhs.clone(),
                    SetOption::All => self.clone(),
                    SetOption::Some(o) => {
                        SetOption::Some(BTreeSet::from_iter(s.intersection(o).cloned()))
                    }
                },
            }
        }

        pub fn has_intersection(&self, rhs: &Self) -> bool {
            match self {
                SetOption::None => false,
                SetOption::All => matches!(rhs, SetOption::All | SetOption::Some(_)),
                SetOption::Some(self_set) => match rhs {
                    SetOption::None => false,
                    SetOption::All => true,
                    SetOption::Some(rhs_set) => self_set.iter().any(|item| rhs_set.contains(item)),
                },
            }
        }

        pub fn retain<F>(&mut self, r#fn: F)
        where
            F: FnMut(&T) -> bool,
        {
            match self {
                SetOption::None => {}
                SetOption::All => {
                    warn!("Invalid `retain` call on SetOption::All")
                }
                SetOption::Some(set) => set.retain(r#fn),
            }
        }
    }

    #[pyclass]
    pub struct Port(u32);

    impl TryFrom<u32> for Port {
        type Error = BackendError;

        fn try_from(value: u32) -> Result<Self, Self::Error> {
            if value > 65535 {
                return Err(BackendError::InvalidPortNumber);
            }
            Ok(Port(value))
        }
    }

    // #[pymethods]
    // impl Port {
    //     pub fn get(&self) -> u32 {
    //         self.0.clone()
    //     }
    // }

    pub struct PortRange(Vec<Port>);

    impl TryFrom<Range<u32>> for PortRange {
        type Error = BackendError;

        fn try_from(value: Range<u32>) -> Result<Self, Self::Error> {
            let mut me = Self {
                0: Vec::with_capacity(value.len()),
            };
            for i in value {
                me.0.push(Port::try_from(i)?);
            }
            Ok(me)
        }
    }
}

pub mod types {
    pub type DeviceHandle = i32;
    pub type ActionHandle = i32;
    pub type SchemeHandle = usize;
    pub type ServiceHandle = i32;
    pub type EnablerHandle = i32;

    pub type UserHandle = i32;
}
