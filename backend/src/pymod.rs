use std::sync::Arc;

use crate::entity::prelude::*;
use crate::entity::sea_orm_active_enums::{
    ActionGuard, DbPrivilegeLevel, EnablerType, Impact, Tag,
};
use crate::error::BackendError;
use crate::primitives::shared::enums::{PrivilegeLevel, ProcessIntegrityLevel, TokenType};
use crate::queries::Queries;

use flagset::FlagSet;
use futures::stream::BoxStream;
use futures_util::TryStreamExt;
use pyo3::exceptions::PyRuntimeError;
use pyo3::prelude::*;
use pyo3::pyclass::IterANextOutput;

use crate::entity::sea_orm_active_enums::EnablerImpactType;
use crate::primitives::capabilities::CapabilityTemplate;
use crate::primitives::predicate::predicate_builders::{self, Builder};
use crate::primitives::predicate::Predicate;
use pyo3::{pyclass, pymethods, PyErr, PyResult};

use crate::primitives::adversary::ArtifactSatchel;
use crate::primitives::capabilities::AttackerCapability;
use crate::primitives::service::Service;
use crate::primitives::shared::utils::Port;
use crate::stores::{ActionStore, ServiceStore, Store};
use backend_macros::{
    impl_mixed_predicate, impl_uni_predicate, python_async_stream_impl_for,
    python_async_stream_impl_for_duo, wrap_store_for_python,
};
use futures::stream::StreamExt;
use futures::Stream;
use sea_orm::*;
use std::any::Any;
use std::rc::Rc;
use tokio::sync::Mutex;

python_async_stream_impl_for!(Service);
python_async_stream_impl_for!(i32);
python_async_stream_impl_for_duo!(i32::usize);

wrap_store_for_python!(ActionStore);
wrap_store_for_python!(ServiceStore);

#[pyclass]
#[derive(Clone, Debug)]
struct PyQueries {
    conn: Arc<DatabaseConnection>,
    action_store: ActionStorePy,
    service_store: ServiceStorePy,
    // satchel: Arc<ArtifactSatchel>,
}

#[pyfunction]
fn create_py_query<'py>(py: Python<'py>) -> PyResult<&'py PyAny> {
    dotenvy::dotenv().expect("Could not read `.env` file.");
    std::env::set_var("RUST_LOG", "info");
    std::env::set_var("RUST_BACKTRACE", "1");
    let db_url =
        std::env::var("DATABASE_URL").expect("Environment variable `DATABASE_URL` must be set.");

    pyo3_asyncio::tokio::future_into_py(py, async move {
        let connection_pool = Arc::new(
            Database::connect(ConnectOptions::new(&db_url))
                .await
                .unwrap(),
        );

        match connection_pool.get_database_backend() {
            sea_orm::DatabaseBackend::MySql => Err(PyRuntimeError::new_err(
                "MySQL connection detected, expecting Postgres",
            )),
            sea_orm::DatabaseBackend::Postgres => Ok(PyQueries {
                action_store: ActionStorePy {
                    conn: connection_pool.clone(),
                },
                service_store: ServiceStorePy {
                    conn: connection_pool.clone(),
                },
                conn: connection_pool,
                // satchel: Default::default(),
            }),
            sea_orm::DatabaseBackend::Sqlite => Err(PyRuntimeError::new_err(
                "SQLite connection detected, expecting Postgres",
            )),
        }
    })
}

#[pymethods]
impl PyQueries {
    fn get_service_by_pk<'py>(&self, pk: i32, py: Python<'py>) -> PyResult<&'py PyAny> {
        let conn_held = Arc::clone(&self.conn);
        let func_block = async move {
            match Queries::get_service_by_pk(pk, conn_held.clone()).await {
                Ok(model) => Ok(model),
                Err(e) => Err(PyErr::from(e)),
            }
        };
        pyo3_asyncio::tokio::future_into_py(py, func_block)
    }

    fn stream_services_for_pagan<'py>(&self, py: Python<'py>) -> PyResult<&'py PyAny> {
        let conn_held = Arc::clone(&self.conn);

        pyo3_asyncio::tokio::future_into_py(py, async move {
            let stream = Queries::all_services_into_pagan_view(conn_held.clone()).await;
            Ok(PyAsyncGenService {
                stream: Arc::new(Mutex::new(stream)),
            })
        })
    }

    fn get_applicable_actions_for_pk<'py>(&self, pk: i32, py: Python<'py>) -> PyResult<&'py PyAny> {
        let conn_held = Arc::clone(&self.conn);
        let func_block = async move {
            match Queries::get_applicable_actions_for_service_pk(pk, conn_held.clone()).await {
                Ok(vec) => Ok(vec),
                Err(e) => Err(PyErr::from(e)),
            }
        };

        pyo3_asyncio::tokio::future_into_py(py, func_block)
    }

    // fn get_applicable_actions_for_adversary<'py>(&self, py: Python<'py>) -> PyResult<&'py PyAny> {
    //     let satchel_ref = Arc::clone(&self.satchel);
    //     let conn_owned = Arc::clone(&self.conn);
    //     pyo3_asyncio::tokio::future_into_py(py, async move {
    //         let stream = ActionStore::get_applicable_actions(satchel_ref);
    //         Ok(PyAsyncGenTup_i32_usize {
    //             stream: Arc::new(Mutex::new(stream)),
    //         })
    //     })
    // }
    //
    // pub fn extend_satchel_with(
    //     &mut self,
    //     tokens: &PyList,
    //     capabilities: &PyList,
    //     credentials: &PyList,
    // ) -> PyResult<()> {
    //     match Arc::get_mut(&mut self.satchel) {
    //         None => {Err(PyRuntimeError::new_err("Could not get mutable reference to the satchel due to multiple Arc or Weak references living."))}
    //         Some(satchel) => {satchel.extend_with(tokens, capabilities, credentials)}
    //     }
    // }

    // pub fn compute_best_capabilities(
    //     &self,
    //     device_id: usize,
    //     action_id: i32,
    //     scheme_id: usize,
    // ) -> PyResult<(Option<usize>, Vec<&AttackerCapability>)> {
    //     match self
    //         .satchel
    //         .compute_best_capabilities(&device_id, &action_id, &scheme_id)
    //     {
    //         Ok(res) => Ok(res),
    //         Err(e) => Err(PyErr::from(e)),
    //     }
    // }
}

#[pyclass]
#[derive(serde::Serialize, serde::Deserialize)]
pub struct PyTokenTypeFlagSet {
    inner: FlagSet<TokenType>,
}

#[pymethods]
impl PyTokenTypeFlagSet {
    #[new]
    fn new(val: u64) -> Result<Self, PyErr> {
        match FlagSet::<TokenType>::new(val) {
            Ok(inner) => Ok(Self { inner }),
            Err(e) => Err(BackendError::InvalidBits(e).into()),
        }
    }

    fn contains(&self, other: &Self) -> bool {
        self.inner.contains(other.inner)
    }

    fn decompose(&self) -> Vec<TokenType> {
        self.inner.clone().drain().collect::<Vec<TokenType>>()
    }
}

#[pyclass]
pub enum PyPredicateType {
    Empty,
    Not,
    All,
    Any,
    NoneOf,
    NotJust,
}

// impl_uni_predicate!(TokenType);
// impl_mixed_predicate!(CapabilityTemplate::AttackerCapability);
// impl_uni_predicate!(EnablerImpactType);

#[pymodule]
fn pagan_rs(_py: Python<'_>, m: &PyModule) -> PyResult<()> {
    m.add_class::<PyQueries>()?;
    // m.add_class::<ActionStorePy>()?;
    // m.add_class::<ServiceStorePy>()?;
    m.add_class::<ServiceModel>()?;
    m.add_class::<Service>()?;
    m.add_class::<Port>()?;
    m.add_class::<ActionApplicability>()?;
    m.add_class::<PyAsyncGeni32>()?;
    m.add_class::<PyAsyncGenService>()?;
    m.add_class::<PyTokenTypeFlagSet>()?;
    m.add_class::<ActionGuard>()?;
    m.add_class::<EnablerType>()?;
    m.add_class::<Tag>()?;
    m.add_class::<Impact>()?;
    m.add_class::<DbPrivilegeLevel>()?;
    m.add_class::<ProcessIntegrityLevel>()?;
    m.add_class::<PrivilegeLevel>()?;
    // m.add_class::<PyTokenTypePredicate>()?;
    m.add_function(wrap_pyfunction!(create_py_query, m)?)?;
    Ok(())
}
