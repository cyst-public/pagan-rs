use crate::entity::actions::Relation::ApplicabilityMatrix;
use crate::entity::prelude::*;
use crate::entity::sea_orm_active_enums::*;
use crate::entity::{
    actions, applicability_matrix, enablers, functional_data, service_impacts, service_tags,
    services,
};
use crate::error::BackendError;
use crate::primitives::service::Service;
use async_stream::stream;
use futures::stream::{BoxStream, Stream, TryStreamExt};
use sea_orm::entity::prelude::Expr;
use sea_orm::*;
use serde::de::IntoDeserializer;
use std::collections::BTreeSet;
use std::str::FromStr;
use std::sync::Arc;

use crate::primitives::shared::types::{ActionHandle, ServiceHandle};
use crate::primitives::shared::utils::SetOption;
use backend_macros::{impl_get_count, impl_pk_get, impl_pk_stream, impl_stream_all_items};

pub struct Queries {}

impl Queries {
    impl_pk_stream!(ServiceEnt::service);
    impl_pk_stream!(ActionEnt::action);

    impl_pk_get!(ServiceEnt::ServiceModel::service);
    impl_pk_get!(ActionEnt::ActionModel::action);

    impl_get_count!(ServiceEnt::service);
    impl_get_count!(ActionEnt::action);

    impl_stream_all_items!(ServiceEnt::ServiceModel::service);

    pub async fn get_services_with_tag(
        tag: &str,
        conn: Arc<DatabaseConnection>,
    ) -> Result<Vec<ServiceModel>, BackendError> {
        match Tag::from_str(tag) {
            Ok(inner_tag) => Ok(ServiceEnt::find()
                .join(JoinType::Join, services::Relation::ServiceTags.def())
                .filter(service_tags::Column::Tag.eq(inner_tag))
                .into_model::<ServiceModel>()
                .all(conn.as_ref())
                .await?),
            Err(e) => Err(BackendError::from(e)),
        }
    }

    pub async fn all_services_into_pagan_view(
        conn: Arc<DatabaseConnection>,
    ) -> BoxStream<'static, Result<Service, BackendError>> {
        Box::pin(stream! {
            let conn_owned = Arc::clone(&conn);
            let mut db_stream = ServiceEnt::find()
            .expr(Expr::cust("array_agg(service_tags.tag || '') as tags")) // hacky as hell, but works - must combine it with empty string, otherwise we gert type mismatches
            .expr(Expr::cust("array_agg(service_impacts.impact || '') as impacts"))
            .select_column(functional_data::Column::AutoElevation)
            .select_column(functional_data::Column::ExecutableAccess)
            .select_column(functional_data::Column::Ports)
            .select_column(functional_data::Column::TargetSoftware)
            .join(JoinType::Join, services::Relation::FunctionalData.def())
            .join(JoinType::Join, services::Relation::ServiceTags.def())
            .join(JoinType::Join, services::Relation::ServiceImpacts.def())
            .group_by(services::Column::Id)
            .group_by(functional_data::Column::AutoElevation)
            .group_by(functional_data::Column::ExecutableAccess)
            .group_by(functional_data::Column::Ports)
            .group_by(functional_data::Column::TargetSoftware)
            .into_model::<Service>()
            .stream(conn_owned.as_ref()).await?;

            while let Some(item) = db_stream.try_next().await? {
                yield Ok(item)
            }
        })
    }

    pub async fn get_service_for_pagan_by_pk(
        pk: ServiceHandle,
        conn: Arc<DatabaseConnection>,
    ) -> Result<Option<Service>, BackendError> {
        Ok(ServiceEnt::find_by_id(pk)
            .expr(Expr::cust_with_expr(
                "array_agg($1)",
                service_tags::Column::Tag.to_string(),
            ))
            .expr(Expr::cust_with_expr(
                "array_agg($1)",
                service_impacts::Column::Impact.to_string(),
            ))
            .select_column(functional_data::Column::AutoElevation)
            .select_column(functional_data::Column::ExecutableAccess)
            .select_column(functional_data::Column::Ports)
            .select_column(functional_data::Column::TargetSoftware)
            .join(JoinType::Join, services::Relation::FunctionalData.def())
            .join(JoinType::Join, services::Relation::ServiceTags.def())
            .join(JoinType::Join, services::Relation::ServiceImpacts.def())
            .group_by(services::Column::Id)
            .into_model::<Service>()
            .one(conn.as_ref())
            .await?)
    }

    pub async fn get_applicable_actions_for_service_pk(
        pk: ServiceHandle,
        conn: Arc<DatabaseConnection>,
    ) -> Result<Vec<ActionApplicability>, BackendError> {
        Ok(ApplicabilityMatrixEnt::find()
            .filter(applicability_matrix::Column::ServiceId.eq(pk))
            .select_column(applicability_matrix::Column::ActionId)
            .select_column(applicability_matrix::Column::ServiceId)
            .select_column(applicability_matrix::Column::EnablerId)
            .select_column(applicability_matrix::Column::Guard)
            .select_column(actions::Column::Description)
            .select_column_as(enablers::Column::Type, "enabler_type")
            .select_column(enablers::Column::Details)
            .join(
                JoinType::Join,
                applicability_matrix::Relation::Actions.def(),
            )
            .join(
                JoinType::Join,
                applicability_matrix::Relation::Enablers.def(),
            )
            .into_model::<ActionApplicability>()
            .all(conn.as_ref())
            .await?)
    }

    pub async fn get_services_where_action_is_applicable(
        pk: ActionHandle,
        conn: Arc<DatabaseConnection>,
    ) -> Result<SetOption<ApplicabilityMatrixModel>, BackendError> {
        let services = ApplicabilityMatrixEnt::find()
            .filter(applicability_matrix::Column::ActionId.eq(pk))
            .all(conn.as_ref())
            .await?;

        if services.is_empty() {
            Ok(SetOption::None)
        } else {
            Ok(SetOption::Some(BTreeSet::from_iter(services)))
        }
    }
}
