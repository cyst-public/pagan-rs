use crate::entity::prelude::ActionModel;
use crate::error::BackendError;
use crate::primitives::adversary::ADVERSARY_ACTION_MAP;
use crate::primitives::adversary::{ActionDescription, ArtifactSatchel};
use crate::queries::Queries;
use crate::stores::Store;
use async_stream::stream;
use async_trait::async_trait;

use crate::primitives::shared::types::ActionHandle;
use futures_util::stream::BoxStream;
use sea_orm::DatabaseConnection;
use std::sync::Arc;

#[derive(Clone, Debug)]
pub struct ActionStore {
    conn: Arc<DatabaseConnection>,
}

impl ActionStore {
    pub fn new(conn: Arc<DatabaseConnection>) -> Self {
        Self { conn }
    }

    pub fn get_applicable_actions(
        &self,
        satchel: Arc<ArtifactSatchel>,
    ) -> BoxStream<'static, Result<(ActionHandle, usize), BackendError>> {
        Box::pin(stream! {
            for (action, description) in ADVERSARY_ACTION_MAP.iter() {
                for (idx, usage) in description.uses.iter().enumerate() {
                    if usage
                        .capabilities_required
                        .eval_satchel(Arc::clone(&satchel))?
                        && !usage.except_if.eval_satchel(Arc::clone(&satchel))?
                    {
                        yield Ok((action.to_owned(), idx))
                    }
                }
            }
        })
    }

    pub fn fetch(&self, pk: &i32) -> Result<&'static ActionDescription, BackendError> {
        match ADVERSARY_ACTION_MAP.get(pk) {
            None => Err(BackendError::NoSuchPrimaryKey(
                "Action does not exist".to_string(),
            )),
            Some(action) => Ok(action),
        }
    }
}

#[async_trait]
impl Store for ActionStore {
    type Item = ActionModel;

    async fn handles(&'static self) -> BoxStream<'static, Result<i32, BackendError>> {
        Box::pin(Queries::stream_pks_action(self.conn.clone()).await)
    }

    async fn cardinality(&self) -> Result<u64, BackendError> {
        Queries::get_action_count(self.conn.clone()).await
    }

    async fn get(&self, pk: i32) -> Result<Self::Item, BackendError> {
        match Queries::get_action_by_pk(pk, Arc::clone(&self.conn)).await {
            Ok(maybe_value) => match maybe_value {
                None => Err(BackendError::NoSuchPrimaryKey(
                    "action does not exist".to_string(),
                )),
                Some(value) => Ok(value),
            },
            Err(e) => Err(e),
        }
    }

    async fn handles_consuming(self) -> BoxStream<'static, Result<i32, BackendError>> {
        Box::pin(Queries::stream_pks_action(self.conn.clone()).await)
    }

    async fn cardinality_consuming(self) -> Result<u64, BackendError> {
        Queries::get_action_count(self.conn.clone()).await
    }

    async fn get_consuming(self, pk: i32) -> Result<Self::Item, BackendError> {
        match Queries::get_action_by_pk(pk, self.conn.clone()).await {
            Ok(maybe_value) => match maybe_value {
                None => Err(BackendError::NoSuchPrimaryKey(
                    "action does not exist".to_string(),
                )),
                Some(value) => Ok(value),
            },
            Err(e) => Err(e),
        }
    }

    fn conn(&self) -> &Arc<DatabaseConnection> {
        &self.conn
    }
}

unsafe impl Send for ActionStore {}
unsafe impl Sync for ActionStore {}
