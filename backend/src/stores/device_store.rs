use crate::error::BackendError;
use crate::primitives::device::Device;
use crate::primitives::shared::types::DeviceHandle;
use crate::primitives::shared::utils::Counter;
use std::collections::HashMap;
use std::sync::Arc;

pub struct DeviceStore {
    devices: HashMap<DeviceHandle, Arc<Device>>,
    attacker: Option<DeviceHandle>,
    counter: Counter,
}

impl DeviceStore {
    pub fn devices(&self) -> &HashMap<DeviceHandle, Arc<Device>> {
        &self.devices
    }
    pub fn attacker(&self) -> Option<&DeviceHandle> {
        self.attacker.as_ref()
    }
}

impl DeviceStore {
    pub(super) fn add_device(
        &mut self,
        device: Device,
        attacker: bool,
    ) -> Result<(), BackendError> {
        if attacker {
            match self.attacker {
                None => {}
                Some(_) => {
                    return Err(BackendError::TopologyError(
                        "Can not have multiple attackers".to_string(),
                    ))
                }
            }
        }
        let handle = self.counter.next();
        self.devices.insert(handle, Arc::new(device));
        if attacker {
            self.attacker = Some(handle);
        }
        Ok(())
    }
}

impl Default for DeviceStore {
    fn default() -> Self {
        Self {
            devices: HashMap::new(),
            attacker: None,
            counter: Counter::default(),
        }
    }
}
