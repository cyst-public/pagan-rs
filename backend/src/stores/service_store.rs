use crate::entity::prelude::ApplicabilityMatrixModel;
use crate::error::BackendError;
use crate::primitives::service::Service;
use crate::primitives::shared::types::ActionHandle;
use crate::primitives::shared::utils::SetOption;
use crate::queries::Queries;
use crate::stores::store::Store;
use async_trait::async_trait;
use futures_util::stream::BoxStream;
use log::{info, warn};
use sea_orm::DatabaseConnection;
use std::sync::Arc;

#[derive(Clone, Debug)]
pub struct ServiceStore {
    conn: Arc<DatabaseConnection>,
}

impl ServiceStore {
    pub fn new(conn: Arc<DatabaseConnection>) -> Self {
        Self { conn }
    }

    pub async fn get_services_where_action_is_applicable(
        &self,
        action: ActionHandle,
    ) -> SetOption<ApplicabilityMatrixModel> {
        match Queries::get_services_where_action_is_applicable(action, Arc::clone(&self.conn)).await
        {
            Ok(set) => set,
            Err(err) => {
                warn!("Could not fetch services from database {}", err);
                SetOption::None
            }
        }
    }
}

#[async_trait]
impl Store for ServiceStore {
    type Item = Service;

    async fn handles(&'static self) -> BoxStream<Result<i32, BackendError>> {
        Box::pin(Queries::stream_pks_service(self.conn.clone()).await)
    }

    async fn cardinality(&self) -> Result<u64, BackendError> {
        Queries::get_service_count(self.conn.clone()).await
    }

    async fn get(&self, pk: i32) -> Result<Self::Item, BackendError> {
        match Queries::get_service_for_pagan_by_pk(pk, self.conn.clone()).await {
            Ok(maybe_value) => match maybe_value {
                None => Err(BackendError::NoSuchPrimaryKey(
                    "service does not exist".to_string(),
                )),
                Some(value) => Ok(value),
            },
            Err(e) => Err(e),
        }
    }

    async fn handles_consuming(self) -> BoxStream<'static, Result<i32, BackendError>> {
        Box::pin(Queries::stream_pks_service(self.conn.clone()).await)
    }

    async fn cardinality_consuming(self) -> Result<u64, BackendError> {
        Queries::get_service_count(self.conn.clone()).await
    }

    async fn get_consuming(self, pk: i32) -> Result<Self::Item, BackendError> {
        match Queries::get_service_for_pagan_by_pk(pk, self.conn.clone()).await {
            Ok(maybe_value) => match maybe_value {
                None => Err(BackendError::NoSuchPrimaryKey(
                    "service does not exist".to_string(),
                )),
                Some(value) => Ok(value),
            },
            Err(e) => Err(e),
        }
    }

    fn conn(&self) -> &Arc<DatabaseConnection> {
        &self.conn
    }
}

// unsafe impl Send for ServiceStore {}
// unsafe impl Sync for ServiceStore {}
