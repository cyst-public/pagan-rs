use crate::error::BackendError;
use async_trait::async_trait;
use futures::Stream;
use futures_util::stream::BoxStream;
use sea_orm::DatabaseConnection;
use std::sync::Arc;

#[async_trait]
pub trait Store {
    type Item;
    async fn handles(&'static self) -> BoxStream<Result<i32, BackendError>>;
    async fn cardinality(&self) -> Result<u64, BackendError>;
    async fn get(&self, pk: i32) -> Result<Self::Item, BackendError>;

    async fn handles_consuming(self) -> BoxStream<'static, Result<i32, BackendError>>;
    async fn cardinality_consuming(self) -> Result<u64, BackendError>;
    async fn get_consuming(self, pk: i32) -> Result<Self::Item, BackendError>;
    fn conn(&self) -> &Arc<DatabaseConnection>;
}
