use actix_web::{get, middleware::Logger, web, App, HttpResponse, HttpServer, Responder};
use log::info;

use sea_orm::{ConnectOptions, ConnectionTrait, Database, DatabaseConnection, StreamTrait};

use backend::queries::Queries;

use clap::Parser;

use backend::entity::prelude::ServiceModel;
use backend::error::BackendError;
use backend::primitives::service::Service;
use futures::stream::StreamExt;

fn serialize_model(
    element: Result<impl serde::Serialize, BackendError>,
) -> Result<web::Bytes, BackendError> {
    match element {
        Ok(model) => match serde_json::to_string(&model) {
            Ok(mut json) => {
                json.push('\u{000A}');
                Ok(web::Bytes::from(json))
            }
            Err(e) => {
                info!("{:?}", e);
                Err(BackendError::from(e))
            }
        },
        Err(e) => {
            info!("{:?}", e);
            Err(e)
        }
    }
}

#[derive(serde::Deserialize)]
pub struct PkQuery {
    pk: i32,
}

#[get("/service_by_pk")]
async fn service_by_pk(
    pk_query: web::Query<PkQuery>,
    app_data: web::Data<DatabaseConnection>,
) -> impl Responder {
    match Queries::get_service_by_pk(pk_query.pk, app_data.into_inner().clone()).await {
        Ok(res) => match res {
            Some(model) => HttpResponse::Ok()
                .content_type("json")
                .body(serde_json::to_string(&model).unwrap()),
            None => HttpResponse::NoContent().finish(),
        },
        Err(_e) => {
            info!("{:?}", _e);
            HttpResponse::InternalServerError().finish()
        }
    }
}

#[get("/services_all")]
async fn services_all(app_data: web::Data<DatabaseConnection>) -> HttpResponse {
    let stream = Queries::stream_all_service(app_data.into_inner().clone()).await;
    let processed_stream =
        stream.map(|element: Result<ServiceModel, BackendError>| serialize_model(element));
    HttpResponse::Ok()
        .content_type("jsonl")
        .streaming(processed_stream)
}

#[get("/services_all_for_pagan")]
async fn services_all_for_pagan(app_data: web::Data<DatabaseConnection>) -> HttpResponse {
    let stream = Queries::all_services_into_pagan_view(app_data.into_inner().clone()).await;
    let processed_stream =
        stream.map(|element: Result<Service, BackendError>| serialize_model(element));
    HttpResponse::Ok()
        .content_type("jsonl")
        .streaming(processed_stream)
}

#[derive(serde::Deserialize)]
pub struct TagQuery {
    tag: String,
}

#[get("/service_by_tag")]
async fn service_by_tag(
    tag_query: web::Query<TagQuery>,
    app_data: web::Data<DatabaseConnection>,
) -> impl Responder {
    match Queries::get_services_with_tag(&tag_query.tag, app_data.into_inner().clone()).await {
        Ok(res_vec) => HttpResponse::Ok()
            .content_type("json")
            .body(serde_json::to_string(&res_vec).unwrap()),
        Err(_e) => {
            info!("{:?}", _e);
            HttpResponse::InternalServerError().finish()
        }
    }
}

#[derive(Parser)]
#[command(author, version, about, long_about=None)]
struct Args {
    #[arg(short, long, required=true, value_parser=["localhost", "all"])]
    interface: String,
}

#[tokio::main]
async fn main() -> std::io::Result<()> {
    let args = Args::parse();
    dotenvy::dotenv().expect("Could not read `.env` file.");
    std::env::set_var("RUST_LOG", "info");
    std::env::set_var("RUST_BACKTRACE", "1");
    env_logger::init();
    let db_url =
        std::env::var("DATABASE_URL").expect("Environment variable `DATABASE_URL` must be set.");

    let connection_pool = Database::connect(ConnectOptions::new(&db_url))
        .await
        .expect("Could not connect to database.");

    match connection_pool.get_database_backend() {
        sea_orm::DatabaseBackend::MySql => panic!("MySQL connection detected, expecting Postgres"),
        sea_orm::DatabaseBackend::Postgres => {}
        sea_orm::DatabaseBackend::Sqlite => {
            panic!("SQLite connection detected, expecting Postgres")
        }
    };

    HttpServer::new(move || {
        App::new()
            .app_data(web::Data::new(connection_pool.clone()))
            .wrap(Logger::default())
            .service(service_by_pk)
            .service(service_by_tag)
            .service(services_all)
            .service(services_all_for_pagan)
    })
    .bind((
        match args.interface.as_str() {
            "localhost" => "127.0.0.1",
            "all" => "0.0.0.0",
            _ => panic!("Invalid interface option"),
        },
        8080,
    ))?
    .run()
    .await
}
