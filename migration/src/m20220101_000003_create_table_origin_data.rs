use super::m20220101_000005_create_table_service::Services;
use sea_orm_migration::prelude::*;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .create_table(
                Table::create()
                    .table(OriginData::Table)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(OriginData::Id)
                            .integer()
                            .not_null()
                            .auto_increment()
                            .primary_key(),
                    )
                    .col(ColumnDef::new(OriginData::Description).string())
                    .col(ColumnDef::new(OriginData::ChangeLog).string())
                    .col(ColumnDef::new(OriginData::Source).string())
                    .col(ColumnDef::new(OriginData::Service).integer().not_null())
                    .to_owned(),
            )
            .await?;

        manager
            .create_foreign_key(
                ForeignKey::create()
                    .name("serviceFK")
                    .from_tbl(OriginData::Table)
                    .from_col(OriginData::Service)
                    .to_tbl(Services::Table)
                    .to_col(Services::Id)
                    .on_update(ForeignKeyAction::Cascade)
                    .on_delete(ForeignKeyAction::Cascade)
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(Table::drop().table(OriginData::Table).to_owned())
            .await
    }
}

#[derive(DeriveIden)]
enum OriginData {
    Table,
    Id,
    Description,
    ChangeLog,
    Source,
    Service,
}
