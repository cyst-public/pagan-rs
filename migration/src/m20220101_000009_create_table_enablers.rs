use sea_orm_migration::{
    prelude::*,
    sea_orm::{EnumIter, Iterable},
    sea_query::extension::postgres::TypeCreateStatement,
};

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .create_type(
                TypeCreateStatement::new()
                    .as_enum(EnablerType::Table)
                    .values(EnablerType::iter().skip(1))
                    .to_owned(),
            )
            .await?;

        manager
            .create_table(
                TableCreateStatement::new()
                    .table(Enablers::Table)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(Enablers::Id)
                            .integer()
                            .not_null()
                            .auto_increment()
                            .primary_key(),
                    )
                    .col(
                        ColumnDef::new(Enablers::Type)
                            .enumeration(EnablerType::Table, EnablerType::iter().skip(1)),
                    )
                    .col(ColumnDef::new(Enablers::Details).json())
                    .col(ColumnDef::new(Enablers::MainCPE).string().not_null())
                    .col(ColumnDef::new(Enablers::Config).array(ColumnType::String(Some(128))))
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(Table::drop().table(Enablers::Table).to_owned())
            .await?;

        manager
            .drop_table(Table::drop().table(EnablerType::Table).to_owned())
            .await
    }
}

#[derive(DeriveIden, EnumIter)]
pub(crate) enum EnablerType {
    Table,
    CVE,
    CWE,
    Misconfiguration,
    UserError,
}

#[derive(DeriveIden)]
pub(crate) enum Enablers {
    Table,
    Id,
    Type,
    Details,
    MainCPE,
    Config,
}
