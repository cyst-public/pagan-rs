use dotenvy;
use sea_orm_migration::prelude::*;

#[async_std::main]
async fn main() {
    match dotenvy::dotenv() {
        Ok(_) => {
            cli::run_cli(migration::Migrator).await;
        }
        Err(e) => eprintln!("Could not read `.env` file. Aborting. {:?}", e),
    };
}
