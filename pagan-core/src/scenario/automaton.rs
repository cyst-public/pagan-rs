use backend::error::BackendError;
use backend::primitives::capabilities::AttackerCapability;
use backend::primitives::credentials::Credential;
use backend::primitives::shared::artifacts::Token;
use backend::primitives::shared::enums::PrivilegeLevel;
use backend::primitives::shared::types::*;
use petgraph::graph::UnGraph;
use petgraph::graph::{EdgeIndex, NodeIndex};
use std::collections::HashMap;
use std::sync::Arc;

#[derive(PartialEq, Eq, Hash)]
pub struct VertexData {
    device: DeviceHandle,
    attacker_capabilities: Vec<Arc<AttackerCapability>>,
    available_credentials: Vec<Arc<Credential>>,
    attacker_tokens: Vec<Arc<Token>>,
    accepting_state: bool,
}

impl VertexData {
    pub fn new(
        device: DeviceHandle,
        attacker_capabilities: Vec<Arc<AttackerCapability>>,
        available_credentials: Vec<Arc<Credential>>,
        attacker_tokens: Vec<Arc<Token>>,
        accepting_state: bool,
    ) -> Self {
        Self {
            device,
            attacker_capabilities,
            available_credentials,
            attacker_tokens,
            accepting_state,
        }
    }
    pub fn device(&self) -> DeviceHandle {
        self.device
    }
    pub fn attacker_capabilities(&self) -> &Vec<Arc<AttackerCapability>> {
        &self.attacker_capabilities
    }
    pub fn available_credentials(&self) -> &Vec<Arc<Credential>> {
        &self.available_credentials
    }
    pub fn attacker_tokens(&self) -> &Vec<Arc<Token>> {
        &self.attacker_tokens
    }
    pub fn accepting_state(&self) -> bool {
        self.accepting_state
    }
}

pub enum RunAs {
    User(UserHandle),
    Privilege(PrivilegeLevel),
}

pub struct EdgeData {
    action: ActionHandle,
    scheme_index: SchemeHandle,
    runas: RunAs,
    services_and_enablers: Vec<(ServiceHandle, Option<EnablerHandle>)>,
}

impl EdgeData {
    pub fn new(
        action: ActionHandle,
        scheme_index: SchemeHandle,
        runas: RunAs,
        services_and_enablers: Vec<(ServiceHandle, Option<EnablerHandle>)>,
    ) -> Self {
        Self {
            action,
            scheme_index,
            runas,
            services_and_enablers,
        }
    }
    pub fn action(&self) -> ActionHandle {
        self.action
    }
    pub fn scheme_index(&self) -> SchemeHandle {
        self.scheme_index
    }
    pub fn runas(&self) -> &RunAs {
        &self.runas
    }
    pub fn services_and_enablers(&self) -> &Vec<(ServiceHandle, Option<EnablerHandle>)> {
        &self.services_and_enablers
    }
}

#[derive(Default)]
pub struct ScenarioStateMachine {
    graph: UnGraph<Arc<VertexData>, Box<EdgeData>>,
    reverse_data_map: HashMap<Arc<VertexData>, NodeIndex>,
    locked: bool,
}

impl ScenarioStateMachine {
    pub(crate) const ROOT: u32 = 0;

    pub(crate) fn add_vertex(&mut self, data: Arc<VertexData>) -> Result<NodeIndex, BackendError> {
        if self.locked {
            return Err(BackendError::AutomatonBuildingError(
                "Automaton is already locked".to_string(),
            ));
        }
        Ok(match self.reverse_data_map.get_key_value(&data) {
            None => {
                let idx = self.graph.add_node(Arc::clone(&data));
                self.reverse_data_map.insert(data, idx);
                idx
            }
            Some((_, node_handle)) => node_handle.to_owned(),
        })
    }

    pub fn add_edge(
        &mut self,
        from: NodeIndex,
        to: NodeIndex,
        data: Box<EdgeData>,
    ) -> Result<EdgeIndex, BackendError> {
        Ok(self.graph.add_edge(from, to, data))
    }

    pub fn get_node(&self, idx: NodeIndex) -> Result<&VertexData, BackendError> {
        Ok(self
            .graph
            .node_weight(idx)
            .ok_or(BackendError::AutomatonBuildingError(
                "invalid node idx in query".to_string(),
            ))?
            .as_ref())
    }

    pub fn get_edge(&self, idx: EdgeIndex) -> Result<&EdgeData, BackendError> {
        Ok(self
            .graph
            .edge_weight(idx)
            .ok_or(BackendError::AutomatonBuildingError(
                "invalid node idx in query".to_string(),
            ))?
            .as_ref())
    }

    pub fn lock(&mut self) {
        self.locked = true;
    }
}
