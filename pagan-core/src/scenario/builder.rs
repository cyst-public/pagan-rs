use crate::scenario::automaton::{ScenarioStateMachine, VertexData};
use crate::scenario::device_tree::DeviceTree;
use crate::shared::config::ScenarioConfiguration;
use crate::terrain::Topology;
use backend::entity::prelude::ApplicabilityMatrixModel;
use backend::error::BackendError;
use backend::primitives::adversary::{ArtifactSatchel, ADVERSARY_ACTION_MAP};
use backend::primitives::credentials::Credential;
use backend::primitives::shared::types::{ActionHandle, DeviceHandle};
use backend::primitives::shared::utils::SetOption;
use futures::future::join_all;
use futures::stream::StreamExt;
use petgraph::graph::NodeIndex;
use std::collections::HashMap;
use std::sync::{Arc, Weak};
use tokio::sync::Mutex;

#[derive(Debug, Clone)]
enum PathState {
    Successful,
    Failed,
    CutBack,
}

impl PathState {
    pub(crate) fn combinator(accumulator: Self, elem: Self) -> Self {
        match &accumulator {
            PathState::Successful => accumulator,
            PathState::Failed => elem,
            PathState::CutBack => match &elem {
                PathState::Successful => elem,
                _ => accumulator,
            },
        }
    }
}

struct WildCardUse {
    generated_at: NodeIndex,
    used_at: Vec<NodeIndex>,
    binding: bool,
}

impl WildCardUse {
    pub(crate) fn new(generated_at: NodeIndex, binding: bool) -> Self {
        Self {
            generated_at,
            used_at: vec![],
            binding,
        }
    }
    pub(crate) fn generated_at(&self) -> NodeIndex {
        self.generated_at
    }
    pub(crate) fn used_at(&self) -> &Vec<NodeIndex> {
        &self.used_at
    }
    pub(crate) fn binding(&self) -> bool {
        self.binding
    }

    pub(crate) fn add_usage(&mut self, idx: NodeIndex) {
        self.used_at.push(idx)
    }
}

struct Echo {
    state: PathState,
    subtrees: Vec<Weak<Self>>,
}

impl Echo {
    pub(crate) fn new(state: PathState, subtrees: Vec<Weak<Self>>) -> Self {
        Self { state, subtrees }
    }
    pub(crate) fn state(&self) -> &PathState {
        &self.state
    }
    pub(crate) fn subtrees(&self) -> &Vec<Weak<Self>> {
        &self.subtrees
    }

    pub(crate) fn add_subtree(&mut self, subtree: Weak<Self>) {
        self.subtrees.push(subtree)
    }
}

pub struct ScenarioBuilder {
    configuration: ScenarioConfiguration,
    automaton: Mutex<ScenarioStateMachine>,
    wildcard_usages: Mutex<HashMap<Credential, WildCardUse>>,
    echoes: Mutex<HashMap<NodeIndex, Echo>>,
    root: Option<NodeIndex>,
    topology: Arc<Topology>,
    exploration_states: Mutex<HashMap<(NodeIndex, DeviceHandle), Mutex<bool>>>,
}

impl ScenarioBuilder {
    pub fn new(configuration: ScenarioConfiguration, topology: Arc<Topology>) -> Self {
        Self {
            configuration,
            automaton: Mutex::new(ScenarioStateMachine::default()),
            wildcard_usages: Mutex::new(HashMap::new()),
            echoes: Mutex::new(HashMap::new()),
            root: None,
            topology,
            exploration_states: Default::default(),
        }
    }
    pub fn configuration(&self) -> &ScenarioConfiguration {
        &self.configuration
    }
    pub fn automaton(&self) -> &Mutex<ScenarioStateMachine> {
        &self.automaton
    }
    pub fn wildcard_usages(&self) -> &Mutex<HashMap<Credential, WildCardUse>> {
        &self.wildcard_usages
    }
    pub fn echoes(&self) -> &Mutex<HashMap<NodeIndex, Echo>> {
        &self.echoes
    }
    pub fn root(&self) -> Option<NodeIndex> {
        self.root
    }
    pub fn topology(&self) -> &Arc<Topology> {
        &self.topology
    }

    pub async fn build(&mut self) -> Result<(), BackendError> {
        let root_data = Arc::new(VertexData::new(
            self.topology.attacker()?.to_owned(),
            vec![],
            self.configuration
                .available_credentials()
                .iter()
                .map(Arc::clone)
                .collect::<Vec<_>>(),
            self.configuration
                .available_tokens()
                .iter()
                .map(|token| Arc::new(token.clone()))
                .collect::<Vec<_>>(),
            false,
        ));
        self.root = {
            let mut m_lock = self.automaton.lock().await;
            Some(m_lock.add_vertex(root_data)?)
        };
        let mut device_paths = DeviceTree::new(self.configuration.goal().device());
        device_paths.build(
            self.topology.accesses(),
            self.configuration.max_devices_touched(),
            self.topology.attacker()?,
        )?;
        let mut path_jobs = Vec::new();
        match device_paths.paths() {
            None => Err(BackendError::AutomatonBuildingError(
                "Could not generate device tree".to_string(),
            )),
            Some(paths) => {
                for path in paths {
                    path_jobs.push(tokio::task::spawn(async { self.explore_path(path) }));
                }
                let results = join_all(path_jobs).await;
                // TODO : combine results
                Ok(())
            }
        }
        // self.explore_state(&self.root.unwrap()).await
    }

    async fn explore_path(&mut self, path: Vec<DeviceHandle>) -> Result<Echo, BackendError> {
        self.explore_state(self.root.as_ref().unwrap(), &path, 0)
            .await
    }

    async fn explore_state(
        &mut self,
        state: &NodeIndex,
        path: &Vec<DeviceHandle>,
        path_idx: usize,
    ) -> Result<Echo, BackendError> {
        let state_data = {
            let m_lock = self.automaton.lock().await;
            m_lock.get_node(state.to_owned())?
        };

        let target_device = state_data.device();
        let children = Arc::new(Mutex::new(Vec::new()));

        let satchel = Arc::new(ArtifactSatchel::new(
            state_data.attacker_tokens(),
            state_data.attacker_capabilities(),
            state_data.available_credentials(),
            &target_device,
        ));

        // TODO: missing branching on target device - remain same or move to next on path

        self.configuration
            .action_store()
            .get_applicable_actions(satchel)
            .for_each_concurrent(None, |res| async {
                match res {
                    Ok((action, scheme)) => {
                        self.find_new_states_with(action, scheme, Arc::clone(&children), state)
                            .await
                    }
                    Err(e) => {}
                }
            })
            .await;

        Ok(Echo::new(PathState::Failed, vec![]))
    }

    async fn find_new_states_with(
        &mut self,
        action: ActionHandle,
        scheme_idx: usize,
        report_to: Arc<Mutex<Vec<NodeIndex>>>,
        parent_id: &NodeIndex,
    ) -> Result<(), BackendError> {
        let scheme = ADVERSARY_ACTION_MAP
            .get(&action)
            .unwrap()
            .uses()
            .get(&scheme_idx)
            .unwrap();

        let mut applications: SetOption<ApplicabilityMatrixModel> = self
            .configuration
            .service_store()
            .get_services_where_action_is_applicable(action.clone())
            .await;

        applications
    }
}
