use backend::error::BackendError;
use backend::primitives::shared::enums::AccessType;
use backend::primitives::shared::types::DeviceHandle;
use std::collections::{HashMap, HashSet};
use std::hash::{Hash, Hasher};
use std::ops::Generator;

#[derive(PartialEq, Eq)]
pub struct TreeNode {
    depth: i32,
    value: DeviceHandle,
    children: HashSet<TreeNode>,
}

impl Hash for TreeNode {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.depth.hash(state);
        self.value.hash(state);
        for child in &self.children {
            child.hash(state)
        }
    }
}

impl TreeNode {
    pub(crate) fn new(depth: i32, value: DeviceHandle, children: HashSet<TreeNode>) -> Self {
        Self {
            depth,
            value,
            children,
        }
    }

    pub(crate) fn extend_path(
        &mut self,
        accesses: &HashMap<DeviceHandle, Vec<(DeviceHandle, AccessType)>>,
        depth_restriction: i32,
        goal: DeviceHandle,
    ) -> Result<bool, BackendError> {
        if self.depth > depth_restriction {
            return Ok(false);
        }
        if self.depth == depth_restriction {
            return Ok(self.value == goal);
        }
        self.children = accesses
            .get(&self.value)
            .ok_or(BackendError::AutomatonBuildingError(
                "bad device index".to_string(),
            ))?
            .iter()
            .filter_map(|(device, _)| match &self.value == device {
                true => None,
                false => {
                    let mut child =
                        TreeNode::new(&self.depth + 1, device.to_owned(), HashSet::new());
                    match child.extend_path(accesses, depth_restriction, goal) {
                        Ok(res) => match res {
                            true => Some(child),
                            false => None,
                        },
                        Err(_) => None,
                    }
                }
            })
            .collect::<HashSet<TreeNode>>();

        Ok(!self.children.is_empty() || self.value == goal)
    }

    pub(crate) fn report_paths(
        self,
        goal: DeviceHandle,
        mut path: Vec<DeviceHandle>,
    ) -> Box<dyn Iterator<Item = Vec<DeviceHandle>>> {
        let generator = move || {
            path.push(self.value.to_owned());
            if self.value == goal {
                yield path.clone()
            }
            for child in self.children.into_iter() {
                for new_path in child.report_paths(goal, path.clone()) {
                    yield new_path
                }
            }
        };
        Box::new(std::iter::from_generator(generator))
    }
}

pub struct DeviceTree {
    goal: DeviceHandle,
    root: Option<TreeNode>,
}

impl DeviceTree {
    pub(crate) fn new(goal: DeviceHandle) -> Self {
        Self { goal, root: None }
    }

    pub(crate) fn build(
        &mut self,
        accesses: &HashMap<DeviceHandle, Vec<(DeviceHandle, AccessType)>>,
        depth_restriction: i32,
        start_device: &DeviceHandle,
    ) -> Result<bool, BackendError> {
        self.root = Option::from(TreeNode::new(0, start_device.to_owned(), HashSet::new()));
        self.root
            .as_mut()
            .unwrap()
            .extend_path(accesses, depth_restriction, self.goal)
    }

    pub fn paths(self) -> Option<Box<dyn Iterator<Item = Vec<DeviceHandle>>>> {
        match self.root {
            None => None,
            Some(node) => Some(node.report_paths(self.goal, Vec::new())),
        }
    }
}

#[cfg(test)]
mod test {
    use crate::scenario::device_tree::DeviceTree;
    use sin_backend::primitives::shared::enums::AccessType;
    use std::collections::HashMap;

    #[test]
    fn test_paths() {
        let accesses = HashMap::from([
            (0, vec![(1, AccessType::Network)]),
            (
                1,
                vec![
                    (2, AccessType::Network),
                    (3, AccessType::Network),
                    (4, AccessType::Network),
                ],
            ),
            (2, vec![(4, AccessType::Network)]),
            (3, vec![(4, AccessType::Network)]),
            (4, vec![(3, AccessType::Network)]),
        ]);

        let mut device_tree = DeviceTree::new(4);
        device_tree
            .build(&accesses, 5, 0)
            .expect("unexpected error");
        let paths = device_tree.paths().unwrap().collect::<Vec<_>>();
        println!("{:?}", &paths);

        assert!(paths.contains(&vec![0, 1, 4]));
        assert!(paths.contains(&vec![0, 1, 2, 4]));
        assert!(paths.contains(&vec![0, 1, 3, 4]));
        assert!(paths.contains(&vec![0, 1, 4, 3, 4]));

        for path in paths.iter() {
            assert_eq!(path.get(0).unwrap().to_owned(), 0);
            assert_eq!(path.last().unwrap().to_owned(), 4);
            assert!(path.len() <= 6);
        }
    }
}
