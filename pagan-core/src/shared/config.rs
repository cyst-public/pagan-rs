use backend::primitives::capabilities::AttackerCapability;
use backend::primitives::credentials::Credential;
use backend::primitives::predicate::Predicate;
use backend::primitives::shared::artifacts::Token;
use backend::primitives::shared::types::{ActionHandle, DeviceHandle};
use backend::stores::{ActionStore, DeviceStore, EnablerStore, ServiceStore};
use std::collections::HashSet;
use std::sync::Arc;

pub struct Goal {
    device: DeviceHandle,
    action: ActionHandle,
    specifier: Predicate<AttackerCapability>,
}

impl Goal {
    pub fn new(
        device: DeviceHandle,
        action: ActionHandle,
        specifier: Predicate<AttackerCapability>,
    ) -> Self {
        Self {
            device,
            action,
            specifier,
        }
    }
    pub fn device(&self) -> DeviceHandle {
        self.device
    }
    pub fn action(&self) -> ActionHandle {
        self.action
    }
    pub fn specifier(&self) -> &Predicate<AttackerCapability> {
        &self.specifier
    }

    pub fn conforming(
        &self,
        action: ActionHandle,
        device: DeviceHandle,
        capabilities: &Vec<Arc<AttackerCapability>>,
    ) -> bool {
        if self.action != action {
            return false;
        }
        if self.device != device {
            return false;
        }
        match self.specifier.eval(
            &capabilities
                .iter()
                .map(|smart| smart.as_ref())
                .collect::<Vec<_>>(),
        ) {
            Ok(res) => res,
            Err(_) => false,
        }
    }
}

pub struct ScenarioConfiguration {
    service_store: ServiceStore,
    enabler_store: EnablerStore,
    // device_store: DeviceStore,
    action_store: ActionStore,

    // @Starting artifacts
    available_tokens: HashSet<Token>,
    available_credentials: HashSet<Arc<Credential>>,

    goal: Goal,

    max_devices_touched: i32,
    max_action_count: i32,
}

impl ScenarioConfiguration {
    pub fn new(
        service_store: ServiceStore,
        enabler_store: EnablerStore,
        action_store: ActionStore,
        available_tokens: HashSet<Token>,
        available_credentials: HashSet<Arc<Credential>>,
        goal: Goal,
        max_devices_touched: i32,
        max_action_count: i32,
    ) -> Self {
        Self {
            service_store,
            enabler_store,
            action_store,
            available_tokens,
            available_credentials,
            goal,
            max_devices_touched,
            max_action_count,
        }
    }
    pub fn service_store(&self) -> &ServiceStore {
        &self.service_store
    }
    pub fn enabler_store(&self) -> &EnablerStore {
        &self.enabler_store
    }
    // pub fn device_store(&self) -> &DeviceStore {
    //     &self.device_store
    // }
    pub fn action_store(&self) -> &ActionStore {
        &self.action_store
    }
    pub fn available_tokens(&self) -> &HashSet<Token> {
        &self.available_tokens
    }
    pub fn available_credentials(&self) -> &HashSet<Arc<Credential>> {
        &self.available_credentials
    }
    pub fn goal(&self) -> &Goal {
        &self.goal
    }
    pub fn max_devices_touched(&self) -> i32 {
        self.max_devices_touched
    }
    pub fn max_action_count(&self) -> i32 {
        self.max_action_count
    }
}
