use backend::error::BackendError;
use backend::primitives::shared::enums::AccessType;
use backend::primitives::shared::types::DeviceHandle;
use backend::stores::DeviceStore;
use log::error;
use std::collections::HashMap;
use std::process::exit;

pub struct Topology {
    store: DeviceStore,
    accesses: HashMap<DeviceHandle, Vec<(DeviceHandle, AccessType)>>,
}

impl Topology {
    pub fn store(&self) -> &DeviceStore {
        &self.store
    }
    pub fn accesses(&self) -> &HashMap<DeviceHandle, Vec<(DeviceHandle, AccessType)>> {
        &self.accesses
    }
    pub fn attacker(&self) -> Result<&DeviceHandle, BackendError> {
        match self.store.attacker() {
            None => Err(BackendError::TopologyError(
                "Attacker not set but queried through topology.".to_string(),
            )),
            Some(handle) => Ok(handle),
        }
    }
}
