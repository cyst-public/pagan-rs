use clap::{command, Parser};
use dotenvy;
use env_logger;
use log;
use sea_orm::*;
use tokio;
mod mock;
mod realistic;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about=None)]
struct Args {
    #[arg(short, long, required=true, value_parser=["mock", "real"])]
    mode: String,
}

#[tokio::main]
async fn main() -> std::io::Result<()> {
    let args = Args::parse();
    dotenvy::dotenv().expect("Could not read `.env` file.");
    std::env::set_var("RUST_LOG", "info");
    std::env::set_var("RUST_BACKTRACE", "1");
    env_logger::init();
    let db_url =
        std::env::var("DATABASE_URL").expect("Environment variable `DATABASE_URL` must be set.");

    let connection_pool = Database::connect(ConnectOptions::new(&db_url))
        .await
        .expect("Could not connect to database.");

    match connection_pool.get_database_backend() {
        sea_orm::DatabaseBackend::MySql => panic!("MySQL connection detected, expecting Postgres"),
        sea_orm::DatabaseBackend::Postgres => {}
        sea_orm::DatabaseBackend::Sqlite => {
            panic!("SQLite connection detected, expecting Postgres")
        }
    };
    if args.mode == "mock" {
        let services_job = mock::mock(&connection_pool);
        let actions_job = realistic::actions::populate(&connection_pool);

        let (services_res, actions_res) = tokio::join!(services_job, actions_job);
        services_res.unwrap();
        actions_res.unwrap();
    } else if args.mode == "real" {
        todo!()
    }
    Ok(())
}
