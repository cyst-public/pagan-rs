use backend::entity::prelude::{ActionAcM, ActionModel};
use backend::error::BackendError;
use backend::mutations::Mutations;
use backend::primitives::adversary::ADVERSARY_ACTION_MAP;
use sea_orm::{DatabaseConnection, Set};
use serde_json;

pub(crate) async fn populate(db: &DatabaseConnection) -> Result<(), BackendError> {
    let mut active_models = Vec::with_capacity(ADVERSARY_ACTION_MAP.len());
    for (pk, action) in ADVERSARY_ACTION_MAP.iter() {
        active_models.push(ActionModel {
            id: pk.to_owned(),
            description: serde_json::to_string(action)?.parse()?,
        })
    }

    Mutations::insert_multiple_actions(active_models, db).await?;

    Ok(())
}
