use proc_macro::TokenStream;
use quote::{format_ident, quote};
use syn::TypeTuple;

struct TypeName {
    type_name: syn::Ident,
}

impl syn::parse::Parse for TypeName {
    fn parse(input: syn::parse::ParseStream) -> syn::Result<Self> {
        Ok(Self {
            type_name: input.parse()?,
        })
    }
}

struct TypeDuo {
    one: syn::Ident,
    _sep: syn::Token![::],
    two: syn::Ident,
}

impl syn::parse::Parse for TypeDuo {
    fn parse(input: syn::parse::ParseStream) -> syn::Result<Self> {
        Ok(Self {
            one: input.parse()?,
            _sep: input.parse()?,
            two: input.parse()?,
        })
    }
}

struct TypeTriplet {
    one: syn::Ident,
    _sep: syn::Token![::],
    two: syn::Ident,
    _sep2: syn::Token![::],
    three: syn::Ident,
}

impl syn::parse::Parse for TypeTriplet {
    fn parse(input: syn::parse::ParseStream) -> syn::Result<Self> {
        Ok(Self {
            one: input.parse()?,
            _sep: input.parse()?,
            two: input.parse()?,
            _sep2: input.parse()?,
            three: input.parse()?,
        })
    }
}

#[proc_macro]
pub fn impl_pk_stream(input: TokenStream) -> TokenStream {
    let input_struct = syn::parse_macro_input!(input as TypeDuo);
    let entity = &input_struct.one;
    let postfix = &input_struct.two;
    let func_name = format_ident!("stream_pks_{}", postfix);

    let gen = quote! {
        pub async fn #func_name(
            conn: Arc<DatabaseConnection>,
        ) -> BoxStream<'static, Result<i32, BackendError>> {
            Box::pin(stream! {
            let mut db_stream  = #entity::find()
                .select_only()
                .column(services::Column::Id)
                .into_model::<PrimaryKeyView>()
                .stream(conn.as_ref())
                .await?;

                while let Some(item) = db_stream.try_next().await? {
                    yield Ok(item.get())
                }
            })
        }
    };
    gen.into()
}

#[proc_macro]
pub fn impl_pk_get(input: TokenStream) -> TokenStream {
    let input_struct = syn::parse_macro_input!(input as TypeTriplet);
    let entity = &input_struct.one;
    let model = &input_struct.two;
    let postfix = &input_struct.three;
    let func_name = format_ident!("get_{}_by_pk", postfix);

    let gen = quote! {
        pub async fn #func_name(
            pk: i32,
            conn: Arc<DatabaseConnection>,
        ) -> Result<Option<#model>, BackendError>  {
            Ok(#entity::find_by_id(pk)
            .one(conn.as_ref())
            .await?)
        }
    };
    gen.into()
}

#[proc_macro]
pub fn impl_get_count(input: TokenStream) -> TokenStream {
    let input_struct = syn::parse_macro_input!(input as TypeDuo);
    let entity = &input_struct.one;
    let postfix = &input_struct.two;
    let func_name = format_ident!("get_{}_count", postfix);

    let gen = quote! {
        pub async fn #func_name(
        conn: Arc<DatabaseConnection>,
    ) -> Result<u64, BackendError> {
        Ok(#entity::find().count(conn.as_ref()).await?)
    }
    };
    gen.into()
}

#[proc_macro]
pub fn impl_stream_all_items(input: TokenStream) -> TokenStream {
    let input_struct = syn::parse_macro_input!(input as TypeTriplet);
    let entity = &input_struct.one;
    let model = &input_struct.two;
    let postfix = &input_struct.three;
    let func_name = format_ident!("stream_all_{}", postfix);

    let gen = quote! {
        pub async fn #func_name(
        conn: Arc<DatabaseConnection>,
        ) -> impl Stream<Item = Result<#model, BackendError>> {
            stream! {
                let conn_owned = conn.clone();
                let mut db_stream = #entity::find().stream(conn_owned.as_ref()).await?;


                while let Some(item) = db_stream.try_next().await? {
                    yield Ok(item)
                }
            }
        }
    };
    gen.into()
}

#[proc_macro]
pub fn python_async_stream_impl_for(input: TokenStream) -> TokenStream {
    let typ_struct = syn::parse_macro_input!(input as TypeName);
    let typ = &typ_struct.type_name;
    let class_name = format_ident!("PyAsyncGen{}", typ);

    let gen = quote! {
        #[pyclass]
        struct #class_name {
            stream: Arc<Mutex<BoxStream<'static, Result<#typ, BackendError>>>>,
        }

        #[pymethods]
        impl #class_name {
            fn __aiter__(slf: PyRef<Self>) -> PyRef<Self> {
                slf
            }

            fn __await__(slf: PyRef<Self>) -> PyRef<Self> {
                slf
            }

            fn __anext__<'py>(&mut self, py: Python<'py>) -> IterANextOutput<&'py PyAny, Option<PyErr>> {
                let runtime_handle = pyo3_asyncio::tokio::get_runtime().handle();
                let func_block = async move {
                    let mut m_guard = self.stream.lock().await;
                    m_guard.try_next().await
                };
                match runtime_handle.block_on(func_block) {
                    Ok(maybe_model) => match maybe_model {
                        Some(model) => match pyo3_asyncio::tokio::future_into_py(py, async move { Ok(model) }) {
                            Ok(fut) => IterANextOutput::Yield(fut),
                            Err(_) => IterANextOutput::Return(None),
                        },
                        None => IterANextOutput::Return(None),
                    },
                    Err(e) => IterANextOutput::Return(Some(PyErr::from(e))),
                }
            }
        }
    };
    gen.into()
}

#[proc_macro]
pub fn python_async_stream_impl_for_duo(input: TokenStream) -> TokenStream {
    let typ_struct = syn::parse_macro_input!(input as TypeDuo);
    let typ_one = &typ_struct.one;
    let typ_two = &typ_struct.two;
    let class_name = format_ident!("PyAsyncGenTup_{}_{}", typ_one, typ_two);
    let tuple_typedef: TypeTuple =
        syn::parse_str(format!("({}, {})", typ_one, typ_two).as_str()).unwrap();

    let gen = quote! {
        #[pyclass]
        struct #class_name {
            stream: Arc<Mutex<BoxStream<'static, Result<#tuple_typedef, BackendError>>>>,
        }

        #[pymethods]
        impl #class_name {
            fn __aiter__(slf: PyRef<Self>) -> PyRef<Self> {
                slf
            }

            fn __await__(slf: PyRef<Self>) -> PyRef<Self> {
                slf
            }

            fn __anext__<'py>(&mut self, py: Python<'py>) -> IterANextOutput<&'py PyAny, Option<PyErr>> {
                let runtime_handle = pyo3_asyncio::tokio::get_runtime().handle();
                let func_block = async move {
                    let mut m_guard = self.stream.lock().await;
                    m_guard.try_next().await
                };
                match runtime_handle.block_on(func_block) {
                    Ok(maybe_model) => match maybe_model {
                        Some(model) => match pyo3_asyncio::tokio::future_into_py(py, async move { Ok(model) }) {
                            Ok(fut) => IterANextOutput::Yield(fut),
                            Err(_) => IterANextOutput::Return(None),
                        },
                        None => IterANextOutput::Return(None),
                    },
                    Err(e) => IterANextOutput::Return(Some(PyErr::from(e))),
                }
            }
        }
    };
    gen.into()
}

#[proc_macro]
pub fn wrap_store_for_python(input: TokenStream) -> TokenStream {
    let input_struct = syn::parse_macro_input!(input as TypeName);
    let store = &input_struct.type_name;
    let wrapper_name = format_ident!("{}Py", store);

    let gen = quote! {

        #[pyclass]
        #[derive(Clone, Debug)]
        struct #wrapper_name {
            pub conn: Arc<DatabaseConnection>,
        }

        #[pymethods]
        impl #wrapper_name {
            fn handles<'py>(&self, py: Python<'py>) -> PyResult<&'py PyAny> {
                let conn_owned = Arc::clone(&self.conn);
                let func_block = async move {
                    let store = #store::new(conn_owned);
                    Ok(PyAsyncGeni32 {
                        stream: Arc::new(Mutex::new(Box::pin(store.handles_consuming().await))),
                    })
                };
                pyo3_asyncio::tokio::future_into_py(py, func_block)
            }

            fn cardinality<'py>(&self, py: Python<'py>) -> PyResult<&'py PyAny> {
                let conn_owned = Arc::clone(&self.conn);
                pyo3_asyncio::tokio::future_into_py(py, async move {
                    let store = #store::new(conn_owned);
                    match store.cardinality_consuming().await {
                        Ok(res) => Ok(res),
                        Err(e) => Err(PyErr::from(e)),
                    }
                })
            }

            fn get<'py>(&self, pk: i32, py: Python<'py>) -> PyResult<&'py PyAny> {
                let conn_owned = Arc::clone(&self.conn);
                pyo3_asyncio::tokio::future_into_py(py, async move {
                    let store = #store::new(conn_owned);
                    match store.get_consuming(pk).await {
                        Ok(res) => Ok(res),
                        Err(e) => Err(PyErr::from(e)),
                    }
                })
            }
        }
    };
    gen.into()
}

#[proc_macro]
pub fn impl_uni_predicate(input: TokenStream) -> TokenStream {
    let input_struct = syn::parse_macro_input!(input as TypeName);
    let typ = &input_struct.type_name;
    let wrapper_name = format_ident!("Py{}Predicate", typ);

    let gen = quote! {

        #[pyclass]
        pub struct #wrapper_name {
            _inner: Predicate<#typ>,
        }

        #[pymethods]
        impl #wrapper_name {
            pub fn simple(r#type: PyPredicateType, args: Vec<#typ>) -> PyResult<Self> {
                Ok(Self {
                    _inner: match &r#type {
                        PyPredicateType::Empty => Ok(predicate_builders::Empty::build_from(())),
                        PyPredicateType::Not => Err(PyRuntimeError::new_err(
                            "Predicate of type Not can only be constructed from another predicate",
                        )),
                        PyPredicateType::All => Ok(predicate_builders::All::build_from(args.as_slice())),
                        PyPredicateType::Any => Ok(predicate_builders::Any::build_from(args.as_slice())),
                        PyPredicateType::NoneOf => Ok(predicate_builders::NoneOf::build_from(args.as_slice())),
                        PyPredicateType::NotJust => {
                            Ok(predicate_builders::NotJust::build_from(args))
                        }
                    }?,
                })
            }

            pub fn nested(r#type: PyPredicateType, args: Vec<Self>) -> PyResult<Self> {
                Ok(Self {
                    _inner: match &r#type {
                        PyPredicateType::Empty => predicate_builders::Empty::build_from(()),
                        PyPredicateType::Not => predicate_builders::Not::build_from(args.get(0)),
                        PyPredicateType::All => predicate_builders::All::build_from(args.as_slice()),
                        PyPredicateType::Any => predicate_builders::Any::build_from(args.as_slice()),
                        PyPredicateType::NoneOf => predicate_builders::NoneOf::build_from(args.as_slice()),
                        PyPredicateType::NotJust => {
                            predicate_builders::NotJust::build_from(args)
                        }
                    },
                })
            }

            pub fn eval(&self, args: &Vec<#typ>) -> PyResult<bool> {
                match self._inner.eval(args.as_slice()) {
                    Ok(res) => Ok(res),
                    Err(e) => PyErr::from(e),
                }
            }
        }
    };
    gen.into()
}

#[proc_macro]
pub fn impl_mixed_predicate(input: TokenStream) -> TokenStream {
    let input_struct = syn::parse_macro_input!(input as TypeDuo);
    let create_type = &input_struct.one;
    let eval_type = &input_struct.two;
    let wrapper_name = format_ident!("Py{}Predicate", create_type);

    let gen = quote! {

        #[pyclass]
        pub struct #wrapper_name {
            _inner: Predicate<#eval_type>,
        }

        #[pymethods]
        impl #wrapper_name {
            pub fn simple(r#type: PyPredicateType, args: Vec<#create_type>) -> PyResult<Self> {
                Ok(Self {
                    _inner: match &r#type {
                        PyPredicateType::Empty => Ok(predicate_builders::Empty::build_from(())),
                        PyPredicateType::Not => Err(PyRuntimeError::new_err(
                            "Predicate of type Not can only be constructed from another predicate",
                        )),
                        PyPredicateType::All => Ok(predicate_builders::All::build_from(args)),
                        PyPredicateType::Any => Ok(predicate_builders::Any::build_from(args)),
                        PyPredicateType::NoneOf => Ok(predicate_builders::NoneOf::build_from(args)),
                        PyPredicateType::NotJust => {
                            Ok(predicate_builders::NotJust::build_from(args))
                        }
                    }?,
                })
            }

            pub fn nested(r#type: PyPredicateType, args: Vec<Self>) -> PyResult<Self> {
                Ok(Self {
                    _inner: match &r#type {
                        PyPredicateType::Empty => predicate_builders::Empty::build_from(()),
                        PyPredicateType::Not => predicate_builders::Not::build_from(args.get(0)),
                        PyPredicateType::All => predicate_builders::All::build_from(args),
                        PyPredicateType::Any => predicate_builders::Any::build_from(args),
                        PyPredicateType::NoneOf => predicate_builders::NoneOf::build_from(args),
                        PyPredicateType::NotJust => {
                            predicate_builders::NotJust::build_from(args)
                        }
                    },
                })
            }

            pub fn eval(&self, args: &Vec<#eval_type>) -> PyResult<bool> {
                match self._inner.eval(args.iter().collect::<Vec<&#eval_type>>().as_slice()) {
                    Ok(res) => Ok(res),
                    Err(e) => PyErr::from(e),
                }
            }
        }
    };
    gen.into()
}
