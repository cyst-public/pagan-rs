from asyncio import Future
from enum import Enum
from typing import List, reveal_type, Optional, overload, TypeVar, Generic, Self


def create_py_query() -> Future[PyQueries]:
    """
    :return: creates a class through which queries can be sent to the database
    """

T = TypeVar('T')
class PyAsyncGen(Generic[T]):
    """
    Wraps Rust streams as python async generators
    """
    def __aiter__(self): ...
    def __await__(self): ...
    def __anext__(self) -> T: ...

PyAsyncGenService = PyAsyncGen[Service]
PyAsyncGeni32 = PyAsyncGen[int]

class PyQueries:
    """
    Utility class wrapping all query functionality for pagan-core
    """
    def get_service_by_pk(self, pk: int) -> Future[Service]:
        """
        :param pk: primary key into db
        :return: The service view
        """

    def stream_services_for_pagan(self) -> PyAsyncGenService:
        """
        :return: an async iterator over all services in the database
        """

    def get_applicable_actions_for_pk(self, pk: int) -> Future[List[ActionApplicability]]:
        """
        :param pk: service primary key
        :return: vector of ActionApplicability objects
        """


class EnablerType(Enum):
    Cve = "Cve"
    Cwe = "Cwe"
    Misconfiguration = "Misconfiguration"
    UserError = "UserError"



class ActionApplicability:
    service_id: int
    action_id: int
    enabler_id: int
    action_description: 'Json'
    enabler_description: 'Json'
    enabler_type: EnablerType


class DbPrivilegeLevel(Enum):
    Administrator = "Administrator"
    LocalSystem = "LocalSystem"
    Service = "Service"
    User = "User"



class Tag(Enum):
    AdministrativeTool = "administrative_tool"
    Antivirus ="antivirus"
    Archiver = "archiver"
    DevelopmentTool = "development_tool"
    FileBrowser = "file_browser"
    FinanceSoftware = "finance_software"
    Media = "media"
    Messenger = "messenger"
    OfficeApplication ="office_application"
    PasswordManager = "password_manager"
    RemoteAccess = "remote_access"
    SystemComponent = "system_component"
    Teleconference = "teleconference"
    Terminal = "terminal"
    VirtualizationPlatform = "virtualization_platform"
    WebBrowser = "web_browser"
    WebHosting = "web_hosting"
    EmailClient = "email_client"
    Database = "database"
    FileSharing = "file_sharing"
    App = "app"

for typ in Tag:
    reveal_type(typ)


class Impact(Enum):
    ExecuteCode = "ExecuteCode"
    ReadFile = "ReadFile"
    UseNetwork = "UseNetwork"
    WriteFile = "WriteFile"


class CapabilityType(Enum):
    AccessFileSysGeneric = "AccessFileSysGeneric",
    AccessFileSysElevated = "AccessFileSysElevated",
    AccessFileSysShared = "AccessFileSysShared",
    ReadFiles = "ReadFiles",
    WriteFiles = "WriteFiles",
    AccessNetworkAsDomain = "AccessNetworkAsDomain",
    AddUser = "AddUser",
    EditUser = "EditUser",
    DeleteUser = "DeleteUser",
    PersistLogOut = "PersistLogOut",
    CodeExecution = "CodeExecution",
    Shutdown = "Shutdown",
    DomainLogin = "DomainLogin",
    Persist = "Persist",
    Impersonate = "Impersonate",
    ControlDomain = "ControlDomain",
    ConfigLocalMachine = "ConfigLocalMachine",
    ConfigCurrentUser = "ConfigCurrentUser",
    SeDebug = "SeDebug",
    SeLoadDriver = "SeLoadDriver",
    SeCreateSecurityToken = "SeCreateSecurityToken",
    SeImpersonateToken = "SeImpersonateToken",
    UacElevate = "UacElevate",




class Port:
    _value: int

    def get(self) -> int:
        """
        :return: The inner port value
        """

class PrivilegeLevel(Enum):
    NONE = 0
    Service = 1
    User = 2
    Administrator = 3
    LocalSystem = 4

class ProcessIntegrityLevel(Enum):
    Low = 1
    Medium = 2
    High = 3
    System = 4
    Protected = 100

class Service:
    id: int
    name: str
    version: str
    vendor: str
    auto_elevation: Optional[bool]
    executable_access: Optional[DbPrivilegeLevel]
    target_software: Optional[str]
    ports: Optional[List[int]]
    tags: List[Tag]
    impacts: List[Impact]

    def get_effective_privilege(self) -> int:
        """
        :return: Computes the running privilege level of the executable based on access and autoElevation properties
        """

    def get_running_integrity(self, spawner_privilege: PrivilegeLevel) -> int:
        """
        :param spawner_privilege: The privilege of the user spawning the process.
        :return: The integrity level of the running process.
        """

    @staticmethod
    def from_json(json: str) -> Self:
        """
        :return:  deserialized Self
        """

    def impacts_as_capabilities(self) -> List[CapabilityType]: